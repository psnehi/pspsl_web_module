﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using PSPCL_Web_Module.InfraStructure.EF.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Infrastructure;

namespace PSPCL_Web_Module.InfraStructure.EF
{
    public class PSPCL_LiveContext : IdentityDbContext<ApplicationUser>
    {
        public PSPCL_LiveContext() : base("PSPCL_LiveContext", throwIfV1Schema: false)
        {
            var adapter = (IObjectContextAdapter)this;
            var objectContext = adapter.ObjectContext;
            objectContext.CommandTimeout = 2 * 60; // value in seconds
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //Database.SetInitializer<PSPCL_LiveContext>(null);
            //modelBuilder.Entity<Transactions>().ToTable("Transactions").HasKey(x => x.ID);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
        public virtual DbSet<UserMaster> UserMaster { get; set; }
        public virtual DbSet<Transactions> Transactions { get; set; }
        public virtual DbSet<SiteList> SiteList { get; set; }
        public virtual DbSet<PSPCL_KSK_BATCH_DTLS> PSPCL_KSK_BATCH_DTLS { get; set; }

        public static PSPCL_LiveContext Create()
        {
            return new PSPCL_LiveContext();
        }        
    }
}
