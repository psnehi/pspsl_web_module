﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.InfraStructure.EF.Entity
{
    public class PSPCL_KSK_BATCH_DTLS
    {
        [Key]
        public int Id { get; set; }
        [StringLength(10)]
        public string TSI_Site_Id { get; set; }
        [StringLength(10)]
        public string PSPCL_KSK_ID { get; set; }
        public DateTime Batch_Close_Date { get; set; }
        public int Batch_No { get; set; }
        public int Start_Rcpt { get; set; }
        public int End_Rcpt { get; set; }
        public decimal Cash_Bills { get; set; }
        public decimal Cash_Amount { get; set; }
        public decimal Chq_Bills { get; set; }
        public decimal Chq_Amount { get; set; }
        public decimal Total_Bills { get; set; }
        public decimal Total_Amount { get; set; }
        public int token_id { get; set; }
        public DateTime DLM { get; set; }
        [StringLength(10)]
        public string UPDATIONSTATUS { get; set; }
    }

}
