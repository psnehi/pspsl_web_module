﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.InfraStructure.EF.Entity
{
    public class SiteList
    {
        [Key]
        public int ID { get; set; }
        [StringLength(255)]
        public string SiteID { get; set; }
        public int? SrNo { get; set; }
        [StringLength(255)]
        public string MachineID { get; set; }
        [StringLength(255)]
        public string Mode { get; set; }
        [StringLength(255)]
        public string City { get; set; }
        [StringLength(255)]
        public string Owner { get; set; }
        [StringLength(255)]
        public string Distt { get; set; }
        [StringLength(255)]
        public string KioskAddress { get; set; }
        public DateTime? LiveDate { get; set; }
        [StringLength(255)]
        public string SerialNo { get; set; }
        [StringLength(255)]
        public string ForbesReplacedSite { get; set; }
        [StringLength(255)]
        public string MachineStatus { get; set; }
    }

}
