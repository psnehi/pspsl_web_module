﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.InfraStructure.EF.Entity
{
    public class Transactions
    {
        [Key]
        public int ID { get; set; }
        [StringLength(1)]
        public string Mode { get; set; }
        [StringLength(50)]
        public string TerminalID { get; set; }
        [StringLength(50)]
        public string USP { get; set; }        
        public decimal? RCPTNO { get; set; }
        [StringLength(50)]
        public string RCPTREF { get; set; }
        public decimal? BATCHNO { get; set; }
        [StringLength(1)]
        public string PAYMENTMODE { get; set; }
        public DateTime? PAYMENTDATE { get; set; }
        public decimal? BILLAMOUNT { get; set; }
        public decimal? COLLECTIONAMT { get; set; }
        public decimal? BATCHPAYMODEAMT { get; set; }
        [StringLength(20)]
        public string REFERENCE1 { get; set; }
        [StringLength(50)]
        public string REFERENCE2 { get; set; }
        [StringLength(50)]
        public string REFERENCE3 { get; set; }
        [StringLength(50)]
        public string REFERENCE4 { get; set; }
        [StringLength(50)]
        public string REFERENCE5 { get; set; }
        [StringLength(50)]
        public string REFERENCE6 { get; set; }
        [StringLength(50)]
        public string REFERENCE7 { get; set; }
        [StringLength(50)]
        public string REFERENCE8 { get; set; }
        [StringLength(50)]
        public string REFERENCE9 { get; set; }
        [StringLength(50)]
        public string REFERENCE10 { get; set; }
        public DateTime? REFERENCEDATE1 { get; set; }
        public DateTime? REFERENCEDATE2 { get; set; }
        public long? REFERENCEINT1 { get; set; }
        public long? REFERENCEINT2 { get; set; }
        public decimal? REFERENCEDECIMAL1 { get; set; }
        public decimal? REFERENCEDECIMAL2 { get; set; }
        public decimal? Denom1 { get; set; }
        public decimal? Denom2 { get; set; }
        public decimal? Denom5 { get; set; }
        public decimal? Denom10 { get; set; }
        public decimal? Denom20 { get; set; }
        public decimal? Denom50 { get; set; }
        public decimal? Denom100 { get; set; }
        public decimal? Denom200 { get; set; }
        public decimal? Denom500 { get; set; }
        public decimal? Denom1000 { get; set; }
        public decimal? Denom2000 { get; set; }
        public decimal? Coin1 { get; set; }
        public decimal? Coin2 { get; set; }
        public decimal? Coin5 { get; set; }
        public decimal? Coin10 { get; set; }
        [StringLength(6)]
        public string CHEQUENO { get; set; }
        [StringLength(50)]
        public string CHEQUEDT { get; set; }
        [StringLength(50)]
        public string MICR { get; set; }
        [StringLength(9)]
        public string BANKCODE { get; set; }
        [StringLength(2)]
        public string TrType { get; set; }
        [StringLength(10)]
        public string BR_CODE { get; set; }
        [StringLength(9)]
        public string BankShortName { get; set; }
        [StringLength(200)]
        public string BankName { get; set; }
        [StringLength(2)]
        public string ACTYPE { get; set; }
        public bool? PostDated { get; set; }
        public bool? ChequeReturned { get; set; }
        public byte[] FrontImage { get; set; }
        [StringLength(50)]
        public string ImageName { get; set; }
        public decimal? ImageSize { get; set; }
        [StringLength(2)]
        public string ReportStatus { get; set; }
        [StringLength(50)]
        public string UPDATIONSTATUS { get; set; }
        [StringLength(200)]
        public string UpdationMessage { get; set; }
        public long? ReportBatchNo { get; set; }
        public DateTime? CreatedOn { get; set; }
        [StringLength(30)]
        public string CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        [StringLength(30)]
        public string UpdatedBy { get; set; }
        [StringLength(200)]
        public string ENACCNO { get; set; }
        [StringLength(200)]
        public string ENCOLLAMT { get; set; }
        public int update_token { get; set; }
        [StringLength(50)]
        public string Ed { get; set; }
        [StringLength(50)]
        public string Idf { get; set; }
        [StringLength(50)]
        public string BillCyc { get; set; }
        [StringLength(50)]
        public string BillGrp { get; set; }
        [StringLength(50)]
        public string ApprovalCode { get; set; }
        [StringLength(50)]
        public string CardNo { get; set; }
        [StringLength(50)]
        public string ExpDate { get; set; }
        [StringLength(50)]
        public string CardType { get; set; }
        [StringLength(50)]
        public string InvoiceNo { get; set; }
        public long? mobileno { get; set; }
        [StringLength(50)]
        public string mailid { get; set; }
    }

}
