﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.InfraStructure.EF.Entity
{
    public class ApplicationUser : IdentityUser
    {
        public string Password { get; set; }
        public string Location { get; set; }
        public int Status { get; set; }
        public byte[] ProfileImage { get; set; }
        public DateTime CreatedOn { get; set; }
        [MaxLength(200)]
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        [MaxLength(200)]
        public string ModifiedBy { get; set; }
        [MaxLength(200)]
        public string Name { get; set; }


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here

            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Sid, UserName),
                new Claim(ClaimTypes.Email, Email),
                new Claim(ClaimTypes.Name, Name),
                new Claim("FullName", this.Name)
            };

            userIdentity.AddClaims(claims);

            return userIdentity;
        }
    }

}
