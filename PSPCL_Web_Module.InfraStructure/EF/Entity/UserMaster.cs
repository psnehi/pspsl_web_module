﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.InfraStructure.EF.Entity
{
    public class UserMaster
    {
        [Key]
        public int ID { get; set; }
        [StringLength(200)]
        public string UserName { get; set; }
        [StringLength(200)]
        public string Password { get; set; }
        [StringLength(200)]
        public string UserType { get; set; }
        [StringLength(200)]
        public string EmailID { get; set; }
        [StringLength(200)]
        public string MobileNo { get; set; }
        public string AuthorizeSite { get; set; }
        public DateTime? CreatedDate { get; set; }
        [StringLength(200)]
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        [StringLength(200)]
        public string ModifiedBy { get; set; }
        public bool? IsActive { get; set; }
    }

}
