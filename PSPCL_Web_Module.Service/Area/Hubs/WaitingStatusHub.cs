﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using PSPCL_Web_Module.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Service.Area.Hubs
{
    [HubName("WaitingStatusHub")]
    public class WaitingStatusHub : Hub
    {
        public async Task SendStatus(string total, string remaining, int percentage, string loginUser)
        {
            try
            {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<WaitingStatusHub>();
                await hubContext.Clients.All.sendWaitingStatus(total, remaining, percentage, loginUser);
            }
            catch (Exception ex)
            {
                Utility.WriteLog("UserService", $"GetRecptNumber: Message => {ex.Message}\nInnerException => {ex.InnerException}\nStackTrace => {ex.StackTrace}");
            }
        }

        public override Task OnConnected()
        {
            Groups.Add(Context.ConnectionId, Context.User.Identity.Name);
            return base.OnConnected();
        }
    }
}
