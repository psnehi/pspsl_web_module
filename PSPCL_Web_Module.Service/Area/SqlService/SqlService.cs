﻿using PSPCL_Web_Module.Dto.Areas.Admin;
using PSPCL_Web_Module.Service.Area.SqlService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PSPCL_Web_Module.InfraStructure.EF;
using PSPCL_Web_Module.Service.Area.ApiManager.Interface;
using PSPCL_Web_Module.Utilities;
using PSPCL_Web_Module.Models.Area.Admin;
using System.Globalization;
using System.Data.SqlClient;
using System.Data;

namespace PSPCL_Web_Module.Service.Area.SqlService
{
    public class SqlService : ISqlService
    {
        private readonly PSPCL_LiveContext _dataContext;
        private readonly IApiManagerService _apiService;
        private readonly string _baseUrl = Urls.BaseUrl;
        public SqlService(PSPCL_LiveContext dataContext,
                          IApiManagerService apiService)
        {
            _apiService = apiService;
            _dataContext = dataContext;
        }

        public Task<string> AmountInWord(decimal amount)
        {
            throw new NotImplementedException();
        }

        public Task<decimal> AvailableBalance()
        {
            throw new NotImplementedException();
        }

        public Task<string> GetAvailableBalance()
        {
            throw new NotImplementedException();
        }

        public async Task<List<SpUserDetail>> GetUsers()
        {
            try
            {
                var result = await _dataContext.Database.SqlQuery<SpUserDetail>(SqlConstant.SpUserView).ToListAsync();
                return result;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("GetUsers", $"GetUsersError: Message => {ex.Message}\nInnerException => {ex.InnerException}\nStackTrace: {ex.StackTrace}");
                return new List<SpUserDetail>();
            }
        }

        public async Task<List<SpTransactionDto>> TransactionReport(ReportModel model)
        {
            try
            {

                DateTime from = DateTime.ParseExact(model.FromDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
                DateTime to = DateTime.ParseExact(model.ToDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
                to = to.AddHours(23);
                to = to.AddMinutes(59);
                to = to.AddSeconds(59);


                SqlParameter[] parameters = new SqlParameter[]
                    {
                    new SqlParameter{
                        ParameterName = "@fromDate",
                        SqlDbType = SqlDbType.DateTime,
                        Value = from,
                        Direction = ParameterDirection.Input
                     },
                    new SqlParameter{
                        ParameterName = "@toDate",
                        SqlDbType = SqlDbType.DateTime,
                        Value = to,
                        Direction = ParameterDirection.Input
                     }
                    };

                var result = await _dataContext.Database.SqlQuery<SpTransactionDto>(SqlConstant.SpTransactionReport, parameters: parameters).ToListAsync();
                return result;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("TransactionReport", $"TransactionReportError: Message => {ex.Message}\nInnerException => {ex.InnerException}\nStackTrace: {ex.StackTrace}");
                return new List<SpTransactionDto>();
            }
        }

        public async Task<List<GetTrxnHistoryDto>> TransactionReportPagination(ReportPaginationModel model)
        {
            try
            {

                DateTime from = DateTime.ParseExact(model.FromDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
                DateTime to = DateTime.ParseExact(model.ToDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
                to = to.AddHours(23);
                to = to.AddMinutes(59);
                to = to.AddSeconds(59);


                SqlParameter[] parameters = new SqlParameter[]
                    {
                    new SqlParameter{
                        ParameterName = "@fromDate",
                        SqlDbType = SqlDbType.DateTime,
                        Value = from,
                        Direction = ParameterDirection.Input
                     },
                    new SqlParameter{
                        ParameterName = "@toDate",
                        SqlDbType = SqlDbType.DateTime,
                        Value = to,
                        Direction = ParameterDirection.Input
                     },
                     new SqlParameter{
                        ParameterName = "@consumerNumber",
                        SqlDbType = SqlDbType.VarChar,
                        Value = model.ConsumerNumber,
                        Direction = ParameterDirection.Input
                     },
                     new SqlParameter{
                        ParameterName = "@paymentMode",
                        SqlDbType = SqlDbType.VarChar,
                        Value = model.PaymentMode,
                        Direction = ParameterDirection.Input
                     },
                     new SqlParameter{
                        ParameterName = "@page",
                        SqlDbType = SqlDbType.Int,
                        Value = model.Page,
                        Direction = ParameterDirection.Input
                     },
                     new SqlParameter{
                        ParameterName = "@size",
                        SqlDbType = SqlDbType.Int,
                        Value = model.Size,
                        Direction = ParameterDirection.Input
                     },
                     new SqlParameter{
                        ParameterName = "@sortColumn",
                        SqlDbType = SqlDbType.VarChar,
                        Value = model.SortColumn,
                        Direction = ParameterDirection.Input
                     },
                     new SqlParameter{
                        ParameterName = "@sortOrder",
                        SqlDbType = SqlDbType.VarChar,
                        Value = model.SortOrder,
                        Direction = ParameterDirection.Input
                     },
                     new SqlParameter{
                        ParameterName = "@totalRows",
                        SqlDbType = SqlDbType.VarChar,
                        Value = model.TotalRecords,
                        Direction = ParameterDirection.Output
                     }};

                var result = await _dataContext.Database.SqlQuery<GetTrxnHistoryDto>(SqlConstant.SpGetTrxnHistory, parameters: parameters).ToListAsync();
                return result;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("TransactionReport", $"TransactionReportError: Message => {ex.Message}\nInnerException => {ex.InnerException}\nStackTrace: {ex.StackTrace}");
                return new List<GetTrxnHistoryDto>();
            }
        }

        public async Task<List<PaymentFileDataDto>> GetPaymentFileData(ReportModel model)
        {
            try
            {
                List<PaymentFileDataDto> dto = new List<PaymentFileDataDto>();
                DateTime from = DateTime.ParseExact(model.FromDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
                DateTime to = DateTime.ParseExact(model.ToDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
                to = to.AddHours(23);
                to = to.AddMinutes(59);
                to = to.AddSeconds(59);


                SqlParameter[] parameters = new SqlParameter[]
                   {
                        new SqlParameter{
                        ParameterName = "@terminalId",
                        SqlDbType = SqlDbType.VarChar,
                        Value = model.TerminalId,
                        Direction = ParameterDirection.Input
                     },
                    new SqlParameter{
                        ParameterName = "@fromDate",
                        SqlDbType = SqlDbType.DateTime,
                        Value = from,
                        Direction = ParameterDirection.Input
                     },
                    new SqlParameter{
                        ParameterName = "@toDate",
                        SqlDbType = SqlDbType.DateTime,
                        Value = to,
                        Direction = ParameterDirection.Input
                     }
                   };

                var result = await _dataContext.Database.SqlQuery<PaymentFileDataDto>(SqlConstant.SpGetNonSapTrxnForPaymentFile, parameters: parameters).ToListAsync();
                return result;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("TransactionReport", $"TransactionReportError: Message => {ex.Message}\nInnerException => {ex.InnerException}\nStackTrace: {ex.StackTrace}");
                return new List<PaymentFileDataDto>();
            }
        }

        public async Task<List<PayFileDaywiseSummary>> GEtPayFileSummary(ReportModel model)
        {
            try
            {
                List<PaymentFileDataDto> dto = new List<PaymentFileDataDto>();
                DateTime from = DateTime.ParseExact(model.FromDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
                DateTime to = DateTime.ParseExact(model.ToDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
                to = to.AddHours(23);
                to = to.AddMinutes(59);
                to = to.AddSeconds(59);


                SqlParameter[] parameters = new SqlParameter[]
                   {
                        new SqlParameter{
                        ParameterName = "@terminalId",
                        SqlDbType = SqlDbType.VarChar,
                        Value = model.TerminalId,
                        Direction = ParameterDirection.Input
                     },
                    new SqlParameter{
                        ParameterName = "@fromDate",
                        SqlDbType = SqlDbType.DateTime,
                        Value = from,
                        Direction = ParameterDirection.Input
                     },
                    new SqlParameter{
                        ParameterName = "@toDate",
                        SqlDbType = SqlDbType.DateTime,
                        Value = to,
                        Direction = ParameterDirection.Input
                     }
                   };

                var result = await _dataContext.Database.SqlQuery<PayFileDaywiseSummary>(SqlConstant.SpGetNonSapPayFilesSummary, parameters: parameters).ToListAsync();
                return result;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("TransactionReport", $"TransactionReportError: Message => {ex.Message}\nInnerException => {ex.InnerException}\nStackTrace: {ex.StackTrace}");
                return new List<PayFileDaywiseSummary>();
            }
        }
    }
}
