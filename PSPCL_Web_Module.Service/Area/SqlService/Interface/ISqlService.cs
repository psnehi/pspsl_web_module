﻿using PSPCL_Web_Module.Dto.Areas.Admin;
using PSPCL_Web_Module.Models.Area.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Service.Area.SqlService.Interface
{
    public interface ISqlService
    {
        Task<string> GetAvailableBalance();
        Task<decimal> AvailableBalance();
        Task<string> AmountInWord(decimal amount);
        Task<List<SpUserDetail>> GetUsers();
        Task<List<SpTransactionDto>> TransactionReport(ReportModel model);
        Task<List<GetTrxnHistoryDto>> TransactionReportPagination(ReportPaginationModel model);
        Task<List<PaymentFileDataDto>> GetPaymentFileData(ReportModel model);
        Task<List<PayFileDaywiseSummary>> GEtPayFileSummary(ReportModel model);
    }
}
