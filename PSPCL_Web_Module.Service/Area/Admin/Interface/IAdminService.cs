﻿using PSPCL_Web_Module.Dto.Areas.Admin;
using PSPCL_Web_Module.Dto.Areas.Common;
using PSPCL_Web_Module.InfraStructure.EF.Entity;
using PSPCL_Web_Module.Models.Area.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Service.Area.Admin.Interface
{
    public interface IAdminService
    {
        //Task<CommonDto> ApproveRequest(string requestId, string loginUser, string remarks);
        //Task<CommonDto> RejectRequest(string requestId, string loginUser, string remarks);
        Task<List<Transactions>> Report(ReportModel model);
        ////Task<byte[]> TransactionExcelReport(ReportModel model);
        Task<string> TotalTransactionDetail();
        Task<CommonDto> AddSite(SiteList model);
        Task<List<SiteList>> SiteList();
        Task<CommonDto> LiveSite(string tsiSiteId, string machineId);
        Task<CommonDto> StopSite(string tsiSiteId, string machineId);
        Task<List<string>> SiteId();
        Task<SiteList> SiteDetail(string tsiSiteId, string machineId);
        Task<CommonDto> UpdateSite(SiteList model);
        Task<CommonDto> AddUser(AddUserModel model);
        Task<List<ReportSummaryDto>> TransactionCount(ReportModel model);
        Task<TransactionDetailCountModel> TransactionDetail();
        //Task<List<DataPoints>> TransactionSummary(ReportModel model);
        Task<List<DataPoints>> TransactionSummaryDetail(ReportModel model);        

        Task<List<UnclearedSummary>> unclearedSummaryService();
        Task<byte[]> TransactionCSVReport(ReportModel model);
        //Task<List<KioskAttendanceDto>> LoginData(ReportModel model);
        //Task<List<kioskAttendanceDto>> LoginData(ReportModel model);
        Task<List<SpUserDetail>> UserList();
        Task<List<SpUserDetail>> SubAdminList();
        Task<ApplicationUser> GetUserDetail(string uniqueId);
        Task<CommonDto> UpdateUser(AddUserModel model);
        Task<CommonDto> ActiveUser(string uniqueId, string loginUser);
        Task<CommonDto> InActiveUser(string uniqueId, string loginUser);
        Task<List<PSPCL_KSK_BATCH_DTLS>> BatchCloseData(ReportModel model);
        Task<List<DataPoints>> TransactionSummary(ReportModel model);

        Task<CommonDto> TransactionCsvReport(ReportModel model, CancellationToken cancellationToken);
        Task<List<SpTransactionDto>> ReportData(ReportModel model);
        Task<List<BatchCloseReview>> BatchCloseReview();
        Task<List<LineChartDataPoints>> monthlyTranTrend();

        Task<List<GetTrxnHistoryDto>> ReportDataPagination(ReportPaginationModel model);


        Task<List<string>> GetNonSapSiteList();
        Task<List<PayFileDaywiseSummary>> GenerateNonSapPayFileSummary(ReportModel model);
        Task<string> GenerateNonSapPaymentFile();
    }
}
