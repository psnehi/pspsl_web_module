﻿using PSPCL_Web_Module.Service.Area.Admin.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PSPCL_Web_Module.InfraStructure.EF;
using PSPCL_Web_Module.Service.Area.SqlService.Interface;
using Microsoft.AspNet.Identity;
using PSPCL_Web_Module.InfraStructure.EF.Entity;
using PSPCL_Web_Module.Dto.Areas.Common;
using PSPCL_Web_Module.Utilities;
using PSPCL_Web_Module.Models.Area.Admin;
using System.Security.Claims;
using System.Data.Entity;
using System.Globalization;
using PSPCL_Web_Module.Dto.Areas.Admin;
using PSPCL_Web_Module.Service.Area.EmailSms.Interface;
using Newtonsoft.Json;
using System.IO;
using System.Data.SqlClient;
using System.Threading;
using PSPCL_Web_Module.Service.Area.Hubs;
using System.Data.Entity.Validation;
using System.IO.Compression;

namespace PSPCL_Web_Module.Service.Area.Admin
{
    public class AdminService : IAdminService
    {

        private readonly PSPCL_LiveContext _PSPCL_LiveContext;
        private readonly IEmailSmsService _emailSmsService;
        private readonly ISqlService _sqlService;
        private readonly UserManager<ApplicationUser> _userManager;

        public AdminService(PSPCL_LiveContext pspcl_liveContext,
                            IEmailSmsService emailSmsService,
                            UserManager<ApplicationUser> userManager,
                            ISqlService sqlService)
        {
            _PSPCL_LiveContext = pspcl_liveContext;
            _userManager = userManager;
            _emailSmsService = emailSmsService;
            _sqlService = sqlService;
        }

        public async Task<CommonDto> AddSite(SiteList model)
        {
            try
            {
                model.MachineStatus = "DEACTIVATED";
                model.Owner = "TSI";
                model.LiveDate = model.LiveDate;
                model.ForbesReplacedSite = "New Site";
                model.SrNo = 0;
                //model.SerialNo = "MK-170059";
                _PSPCL_LiveContext.SiteList.Add(model);
                await _PSPCL_LiveContext.SaveChangesAsync();

                return new CommonDto
                {
                    IsSuccess = true,
                    Message = "Site added successfully"
                };
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
            catch (Exception ex)
            {
                Utility.WriteLog("AddSite --> " + ex.Message);
                Utility.WriteLog("AddSite --> " + ex.StackTrace);
                return new CommonDto
                {
                    IsSuccess = false,
                    Message = "Error, while adding site"
                };
            }
        }

        public async Task<CommonDto> AddUser(AddUserModel model)
        {
            try
            {
                string password = "password";

                var user = new ApplicationUser
                {
                    CreatedBy = $"{model.LoginUsername.ToUpper()} ({model.LoginName.ToUpper()})",
                    CreatedOn = DateTime.Now,
                    Email = model.Email,
                    EmailConfirmed = true,
                    Location = model.Location,
                    PhoneNumber = model.Phone,
                    Status = (int)UserStatus.Inactive,
                    UserName = model.Username,
                    Name = model.Name,
                    Password = SecurityHelper.Encrypt(password)
                };


                var result = await _userManager.CreateAsync(user, password);

                if (result.Succeeded)
                {
                    switch (model.UserRole.ToUpper())
                    {
                        case RoleType.SuperAdmin:
                            await _userManager.AddToRoleAsync(user.Id, RoleType.SuperAdmin);
                            await _userManager.AddClaimAsync(user.Id, new Claim(ClaimTypes.Role, RoleType.SuperAdmin));
                            break;

                        case RoleType.Admin:
                            await _userManager.AddToRoleAsync(user.Id, RoleType.Admin);
                            await _userManager.AddClaimAsync(user.Id, new Claim(ClaimTypes.Role, RoleType.Admin));
                            break;

                        case RoleType.SubAdmin:
                            await _userManager.AddToRoleAsync(user.Id, RoleType.SubAdmin);
                            await _userManager.AddClaimAsync(user.Id, new Claim(ClaimTypes.Role, RoleType.SubAdmin));
                            break;

                        case RoleType.User:
                            await _userManager.AddToRoleAsync(user.Id, RoleType.User);
                            await _userManager.AddClaimAsync(user.Id, new Claim(ClaimTypes.Role, RoleType.User));
                            break;
                    }

                    return new CommonDto
                    {
                        IsSuccess = true,
                        Message = "Register Successfully !!!"
                    };
                }
                else
                {
                    return new CommonDto
                    {
                        IsSuccess = false,
                        Message = $"Error : {result.Errors.ElementAt(0)}"
                    };
                }
            }
            catch (Exception ex)
            {
                return new CommonDto
                {
                    IsSuccess = false,
                    Message = $"Error : {ex.Message}"
                };
            }
        }

        /*
        public Task<CommonDto> ApproveRequest(string requestId, string loginUser, string remarks)
        {
            throw new NotImplementedException();
        }
        */
        public async Task<CommonDto> LiveSite(string tsiSiteId, string kioskId)
        {
            try
            {
                var siteData = await _PSPCL_LiveContext.SiteList.FirstOrDefaultAsync(f => f.SiteID == tsiSiteId && f.MachineID == kioskId);
                siteData.LiveDate = DateTime.Now;
                siteData.MachineStatus = "ACTIVE";
                await _PSPCL_LiveContext.SaveChangesAsync();
                return new CommonDto
                {
                    IsSuccess = true,
                    Message = "Site Activated Successfully"
                };
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Live Site --> " + ex.Message);
                Utility.WriteLog("Live Site --> " + ex.StackTrace);
                return new CommonDto
                {
                    IsSuccess = false,
                    Message = "Error, Server problem"
                };
            }
        }

        /*
        public Task<CommonDto> RejectRequest(string requestId, string loginUser, string remarks)
        {
            throw new NotImplementedException();
        }
        */
        public async Task<List<Transactions>> Report(ReportModel model)
        {
            try
            {
                DateTime from = DateTime.ParseExact(model.FromDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
                DateTime to = DateTime.ParseExact(model.ToDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
                to = to.AddHours(23);
                to = to.AddMinutes(59);
                to = to.AddSeconds(59);

                List<Transactions> transactionsData = new List<Transactions>();
                switch (model.Type)
                {
                    case "ALL":
                        if (model.TerminalId != null)
                            transactionsData = await _PSPCL_LiveContext.Transactions.Where(w => w.Mode == "L" && w.PAYMENTDATE >= from && w.PAYMENTDATE <= to && w.TerminalID.ToUpper() == model.TerminalId).OrderByDescending(o => o.PAYMENTDATE).ToListAsync();
                        else
                            transactionsData = await _PSPCL_LiveContext.Transactions.Where(w => w.Mode == "L" && w.PAYMENTDATE >= from && w.PAYMENTDATE <= to).OrderByDescending(o => o.PAYMENTDATE).ToListAsync();
                        break;

                    case "CASH":
                        if (model.TerminalId != null)
                            transactionsData = await _PSPCL_LiveContext.Transactions.Where(w => w.Mode == "L" && w.PAYMENTDATE >= from && w.PAYMENTDATE <= to && w.TerminalID.ToUpper() == model.TerminalId && w.PAYMENTMODE == "C").OrderByDescending(o => o.PAYMENTDATE).ToListAsync();
                        else
                            transactionsData = await _PSPCL_LiveContext.Transactions.Where(w => w.Mode == "L" && w.PAYMENTDATE >= from && w.PAYMENTDATE <= to && w.PAYMENTMODE == "C").OrderByDescending(o => o.PAYMENTDATE).ToListAsync();
                        break;

                    case "CHEQUE":
                        if (model.TerminalId != null)
                            transactionsData = await _PSPCL_LiveContext.Transactions.Where(w => w.Mode == "L" && w.PAYMENTDATE >= from && w.PAYMENTDATE <= to && w.TerminalID.ToUpper() == model.TerminalId && (w.PAYMENTMODE == "A" || w.PAYMENTMODE == "Q" || w.PAYMENTMODE == "D")).OrderByDescending(o => o.PAYMENTDATE).ToListAsync();
                        else
                            transactionsData = await _PSPCL_LiveContext.Transactions.Where(w => w.Mode == "L" && w.PAYMENTDATE >= from && w.PAYMENTDATE <= to && (w.PAYMENTMODE == "A" || w.PAYMENTMODE == "Q" || w.PAYMENTMODE == "D")).OrderByDescending(o => o.PAYMENTDATE).ToListAsync();
                        break;

                    case "CARD":
                        if (model.TerminalId != null)
                            transactionsData = await _PSPCL_LiveContext.Transactions.Where(w => w.Mode == "L" && w.PAYMENTDATE >= from && w.PAYMENTDATE <= to && w.TerminalID.ToUpper() == model.TerminalId && w.PAYMENTMODE == "R").OrderByDescending(o => o.PAYMENTDATE).ToListAsync();
                        else
                            transactionsData = await _PSPCL_LiveContext.Transactions.Where(w => w.Mode == "L" && w.PAYMENTDATE >= from && w.PAYMENTDATE <= to && w.PAYMENTMODE == "R").OrderByDescending(o => o.PAYMENTDATE).ToListAsync();
                        break;

                    default:
                        if (model.TerminalId != null)
                            transactionsData = await _PSPCL_LiveContext.Transactions.Where(w => w.Mode == "L" && w.TrType == model.Type && w.TerminalID == model.TerminalId && w.PAYMENTDATE >= from && w.PAYMENTDATE <= to).OrderByDescending(o => o.PAYMENTDATE).ToListAsync();
                        else
                            transactionsData = await _PSPCL_LiveContext.Transactions.Where(w => w.Mode == "L" && w.TrType == model.Type && w.PAYMENTDATE >= from && w.PAYMENTDATE <= to).OrderByDescending(o => o.PAYMENTDATE).ToListAsync();
                        break;

                }
                foreach (var item in transactionsData)
                {
                    if (item.PAYMENTMODE == "C" || item.PAYMENTMODE == "E")
                        item.PAYMENTMODE = "Cash";
                    else if (item.PAYMENTMODE == "A" || item.PAYMENTMODE == "Q" || item.PAYMENTMODE == "D")
                        item.PAYMENTMODE = "Cheque";
                    else if (item.PAYMENTMODE == "R")
                        item.PAYMENTMODE = "Card";
                }
                return transactionsData;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Report --> " + ex.Message);
                Utility.WriteLog("Report --> " + ex.StackTrace);
                return new List<Transactions>();
            }
        }

        public async Task<SiteList> SiteDetail(string tsiSiteId, string machineId)
        {
            return await _PSPCL_LiveContext.SiteList.FirstOrDefaultAsync(f => f.SiteID.ToUpper() == tsiSiteId.ToUpper() && f.MachineID == machineId.ToUpper());
        }

        //public async Task<List<string>> SiteId() => await _PSPCL_LiveContext.SiteList.Select(s => s.MachineID).ToListAsync();
        public async Task<List<string>> SiteId()
        {
            try
            {
                var SiteList = await _PSPCL_LiveContext.SiteList.Select(s => s.MachineID).ToListAsync();
                return SiteList;
            }
            catch (Exception ex)
            {
                List<string> SiteList = new List<string>();
                SiteList.Add($"{ex.Message}");
                return SiteList;
            }

        }

        public async Task<List<SiteList>> SiteList()
        {
            return await _PSPCL_LiveContext.SiteList.ToListAsync();
        }

        public async Task<CommonDto> StopSite(string tsiSiteId, string machineId)
        {
            try
            {
                var siteData = await _PSPCL_LiveContext.SiteList.FirstOrDefaultAsync(f => f.SiteID == tsiSiteId && f.MachineID == machineId);
                //siteData.LiveDate = DateTime.Now;
                siteData.MachineStatus = "INACTIVE";
                await _PSPCL_LiveContext.SaveChangesAsync();
                return new CommonDto
                {
                    IsSuccess = true,
                    Message = "Site DeActivated Successfully"
                };
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Stop Site --> " + ex.Message);
                Utility.WriteLog("Stop Site --> " + ex.StackTrace);
                return new CommonDto
                {
                    IsSuccess = false,
                    Message = "Error, Server problem"
                };
            }
        }

        public async Task<string> TotalTransactionDetail()
        {
            try
            {

                var data = await _PSPCL_LiveContext.Transactions.Select(s => new { Mode = s.Mode, Collectionamt = s.COLLECTIONAMT }).Where(c => c.Mode == "L").ToListAsync();
                var count = String.Format("{0:n0}", data.Count);
                var amount = String.Format(new CultureInfo("en-IN"), "{0:n}", data.Sum(s => s.Collectionamt));
                return $"{count}/{(amount == "" ? "0.00" : amount)}";
            }
            catch (Exception ex)
            {
                Utility.WriteLog("TotalTransactionDetail --> " + ex.StackTrace);
                Utility.WriteLog("TotalTransactionDetail --> " + ex.Message);
                return string.Empty;
            }
        }

        public async Task<CommonDto> UpdateSite(SiteList model)
        {
            try
            {
                var updateData = await _PSPCL_LiveContext.SiteList.FirstOrDefaultAsync(f => f.SiteID.ToUpper() == model.SiteID.ToUpper() && f.MachineID == model.MachineID);

                //updateData.City = model.City;
                //updateData.Pincode = model.Pincode;
                if (model.LiveDate != null)
                    updateData.LiveDate = model.LiveDate;
                updateData.SerialNo = model.SerialNo;
                updateData.Distt = model.Distt;
                updateData.City = model.City;
                updateData.KioskAddress = model.KioskAddress;
                updateData.Mode = model.Mode;
                //updateData.Address2 = model.Address2;

                await _PSPCL_LiveContext.SaveChangesAsync();

                return new CommonDto
                {
                    IsSuccess = true,
                    Message = "Site Detail Updated Successfully"
                };
            }
            catch (Exception ex)
            {
                Utility.WriteLog("TopUpRequest --> " + ex.Message);
                Utility.WriteLog("TopUpRequest --> " + ex.StackTrace);

                return new CommonDto
                {
                    IsSuccess = false,
                    Message = $"Error : {ex.Message}"
                };
            }
        }

        public async Task<List<ReportSummaryDto>> TransactionCount(ReportModel model)
        {
            try
            {
                //DateTime from = DateTime.ParseExact(model.FromDate.Replace('-', '/'), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //DateTime to = DateTime.ParseExact(model.ToDate.Replace('-', '/'), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime from = DateTime.ParseExact(model.FromDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
                DateTime to = DateTime.ParseExact(model.ToDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
                to = to.AddHours(23);
                to = to.AddMinutes(59);
                to = to.AddSeconds(59);

                return await _PSPCL_LiveContext.Transactions.Where(w => w.Mode == "L" && w.PAYMENTDATE >= from && w.PAYMENTDATE <= to).GroupBy(g => g.TerminalID).Select(s => new ReportSummaryDto { Count = s.Count(), TerminalId = s.Key }).ToListAsync();
            }
            catch (Exception ex)
            {
                Utility.WriteLog("TransactionCount --> " + ex.Message);
                Utility.WriteLog("TransactionCount --> " + ex.StackTrace);
                return new List<ReportSummaryDto>();
            }
        }

        public async Task<TransactionDetailCountModel> TransactionDetail()
        {
            try
            {
                DateTime currentDate = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy").Replace('-', '/'), "dd/MM/yyyy", CultureInfo.InvariantCulture);

                /*
                DateTime currentMonth = currentDate.AddDays(-currentDate.Day + 1);
                DateTime currentYear = new DateTime(DateTime.Now.Year, 1, 01, 00, 00, 00);

                var data = await _PSPCL_LiveContext.Transactions
                                                   .Where(w => w.PAYMENTDATE >= currentYear)
                                                   .Select(s => new
                                                   {
                                                       s.COLLECTIONAMT,
                                                       s.PAYMENTDATE
                                                   })
                                                   .ToListAsync();
                return new TransactionDetailCountModel
                {
                    //YearCount = $"{String.Format("{0:n0}", data.Count)}/{String.Format(new CultureInfo("en-IN"), "{0:n}", data.Sum(s => s.COLLECTIONAMT))}",
                    //MonthCount = $"{String.Format("{0:n0}", data.Where(x => x.PAYMENTDATE >= currentMonth).Count())}/{String.Format(new CultureInfo("en-IN"), "{0:n}", data.Where(w => w.PAYMENTDATE >= currentMonth).Sum(s => s.COLLECTIONAMT))}",
                    //TodayCount = $"{String.Format("{0:n0}", data.Where(w => w.PAYMENTDATE >= currentDate).Count())}/{String.Format(new CultureInfo("en-IN"), "{0:n}", data.Where(w => w.PAYMENTDATE >= currentDate).Sum(s => s.COLLECTIONAMT))}"
                    YearCount = $"{String.Format("{0:n0}", data.Count)}",
                    MonthCount = $"{String.Format("{0:n0}", data.Where(x => x.PAYMENTDATE >= currentMonth).Count())}",
                    TodayCount = $"{String.Format("{0:n0}", data.Where(w => w.PAYMENTDATE >= currentDate).Count())}"
                };
                */

                TransactionDetailCountModel summary = new TransactionDetailCountModel();
                summary = await _PSPCL_LiveContext.Database.SqlQuery<TransactionDetailCountModel>($"EXEC sp_transactions_count @fromDate",
                        new SqlParameter("@fromDate", currentDate)).FirstOrDefaultAsync();
                return summary;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("TransactionDetail --> " + ex.Message);
                Utility.WriteLog("TransactionDetail --> " + ex.StackTrace);
                return new TransactionDetailCountModel { };
            }
        }

        /*
        public async Task<List<DataPoints>> TransactionSummary(ReportModel model)
        {
            List<DataPoints> dataPoints = new List<DataPoints>();
            try
            {
                DateTime from = DateTime.ParseExact(model.FromDate.Replace('-', '/'), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime to = DateTime.ParseExact(model.ToDate.Replace('-', '/'), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                to = to.AddHours(23);
                to = to.AddMinutes(59);
                to = to.AddSeconds(59);

                try
                {
                    List<Summary> summary = new List<Summary>();
                    summary = await _PSPCL_LiveContext.Database.SqlQuery<Summary>($"EXEC sp_transactions_summary @fromDate,@toDate",
                            new Object[]{ new SqlParameter("@fromDate",from),
                                      new SqlParameter("@toDate",to)}).ToListAsync();

                    foreach (var item in summary)
                    {
                        dataPoints.Add(new DataPoints(item.Paymentmode, Convert.ToDouble(item.Tranx), ""));
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog("AdminService", $"TransactionCount -> {ex.Message}\n{ex.StackTrace}");
                    //throw;
                }
                //return summary;
                //return await _PSPCL_LiveContext.Transactions.Where(w => w.Mode == "L" && w.Paymentdate >= from && w.Paymentdate <= to).GroupBy(g => g.TerminalId).Select(s => new ReportSummaryModel { Count = s.Count(), TerminalId = s.Key }).ToListAsync();
            }
            catch (Exception ex)
            {
                Utility.WriteLog("TransactionCount --> " + ex.Message);
                Utility.WriteLog("TransactionCount --> " + ex.StackTrace);
                //return new List<ReportSummaryModel>();
            }
            return dataPoints;
        }
        */
        public async Task<List<DataPoints>> TransactionSummaryDetail(ReportModel model)
        {
            //List<DataPoints> dataPoints = new List<DataPoints>();
            List<SummaryDetail> summary = new List<SummaryDetail>();
            List<DataPoints> dataPoints = new List<DataPoints>();
            List<DataPoints> dataPointss = new List<DataPoints>();
            try
            {
                DateTime from = DateTime.ParseExact(model.FromDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
                DateTime to = DateTime.ParseExact(model.ToDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
                to = to.AddHours(23);
                to = to.AddMinutes(59);
                to = to.AddSeconds(59);

                summary = await _PSPCL_LiveContext.Database.SqlQuery<SummaryDetail>($"EXEC sp_transactions_summary_new @fromDate,@toDate",
                        new Object[]{ new SqlParameter("@fromDate",from),
                                      new SqlParameter("@toDate",to)}).ToListAsync();

                var tt = summary.GroupBy(g => g.TerminalId).Select(s => new { TerminalId = s.Key, Data = s.ToList() }).ToList();
                tt.ForEach(f =>
                {
                    f.Data.ForEach(ff =>
                    {
                        if (ff.Paymentmode.ToUpper() == "CASH")
                            dataPoints.Add(new DataPoints(f.TerminalId, Convert.ToDouble(ff.Tranx), ff.Paymentmode.ToLower()));
                        else
                            dataPoints.Add(new DataPoints(f.TerminalId, Convert.ToDouble(0), "cash"));

                        if (ff.Paymentmode.ToUpper() == "CHEQUE")
                            dataPoints.Add(new DataPoints(f.TerminalId, Convert.ToDouble(ff.Tranx), ff.Paymentmode.ToLower()));
                        else
                            dataPoints.Add(new DataPoints(f.TerminalId, Convert.ToDouble(0), "cheque"));
                    });
                });


                dataPointss = dataPoints.GroupBy(g => new { label = g.label, Paymode = g.PayMode })
                                    .Select(s => new DataPoints
                                    {
                                        label = s.Key.label,
                                        y = s.Sum(ss => ss.y),
                                        PayMode = s.Key.Paymode,

                                    }).ToList();
            }

            catch (Exception ex)
            {
                Utility.WriteLog("TransactionCount --> " + ex.Message);
                Utility.WriteLog("TransactionCount --> " + ex.StackTrace);
                //return new List<ReportSummaryModel>();
            }
            return dataPointss;
        }


        //public async Task<byte[]> TransactionExcelReport(ReportModel model)
        //{
        //    byte[] result = null;
        //    try
        //    {
        //        List<Transactions> transData = await Report(model);
        //        transData = transData.OrderBy(o => o.PAYMENTDATE).ToList();

        //        string reportFileName = $"{DateTime.Now.ToString("ddMMyyyyHHmmss")}TransactionReport.xlsx";

        //        var dirPath = AppDomain.CurrentDomain.BaseDirectory + "TransactionReport\\";
        //        var reportFilePath = AppDomain.CurrentDomain.BaseDirectory + "TransactionReport\\" + reportFileName;
        //        if (!Directory.Exists(dirPath)) Directory.CreateDirectory(dirPath).Create();

        //        FileInfo sInfo = new FileInfo(reportFilePath);
        //        ExcelPackage pck = new ExcelPackage(sInfo);
        //        ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Transaction Report");
        //        int rowStart = 1;

        //        Color lightGray = ColorTranslator.FromHtml("#eee");
        //        Color gray = ColorTranslator.FromHtml("#bdc3c7");
        //        Color drakGray = ColorTranslator.FromHtml("#bdc3c7");

        //        using (ExcelRange Rng = ws.Cells[rowStart, 1, rowStart, 10])
        //        {
        //            Rng.Value = "Adani Electricity Mumbai (Maharashtra)";
        //            Rng.Merge = true;
        //            Rng.Style.Font.Size = 16;
        //            Rng.Style.Font.Bold = true;
        //            Rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //            Rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //        }
        //        rowStart++;

        //        using (ExcelRange Rng = ws.Cells[rowStart, 1, rowStart, 10])
        //        {
        //            Rng.Value = "Transaction Report";
        //            Rng.Merge = true;
        //            Rng.Style.Font.Size = 12;
        //            Rng.Style.Font.Bold = true;
        //            Rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //            Rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //        }
        //        rowStart++;

        //        using (ExcelRange Rng = ws.Cells[rowStart, 1, rowStart, 4])
        //        {
        //            Rng.Value = $"From Date : {model.FromDate.Replace('-', '/')} To Date : {model.ToDate.Replace('-', '/')}";
        //            Rng.Merge = true;
        //            Rng.Style.Font.Size = 12;
        //            Rng.Style.Font.Bold = true;
        //            Rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //            Rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
        //        }


        //        using (ExcelRange Rng = ws.Cells[rowStart, 5, rowStart, 5])
        //        {
        //            Rng.Value = $"";
        //            Rng.Style.Font.Size = 12;
        //            Rng.Style.Font.Bold = true;
        //            Rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //            Rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
        //        }

        //        using (ExcelRange Rng = ws.Cells[rowStart, 6, rowStart, 10])
        //        {
        //            Rng.Value = $"Report run date & time : {DateTime.Now.ToString("dd-MM-yyyy")}, {DateTime.Now.ToString("HH:mm")} Hrs";
        //            Rng.Merge = true;
        //            Rng.Style.Font.Size = 12;
        //            Rng.Style.Font.Bold = true;
        //            Rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //            Rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
        //        }
        //        rowStart++;

        //        //-----------------------------------------------------------//

        //        ws.Cells["A" + rowStart].Value = "S.No.";
        //        ws.Cells["A" + rowStart].Style.Font.Bold = true;
        //        ws.Cells["A" + rowStart].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //        ws.Cells["A" + rowStart].Style.Fill.BackgroundColor.SetColor(drakGray);
        //        ws.Cells["A" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["A" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //        ws.Cells["A" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["A" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //        ws.Cells["A" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["A" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);

        //        ws.Cells["B" + rowStart].Value = "Terminal Id";
        //        ws.Cells["B" + rowStart].Style.Font.Bold = true;
        //        ws.Cells["B" + rowStart].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //        ws.Cells["B" + rowStart].Style.Fill.BackgroundColor.SetColor(drakGray);
        //        ws.Cells["B" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["B" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //        ws.Cells["B" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["B" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //        ws.Cells["B" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["B" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);

        //        ws.Cells["C" + rowStart].Value = "PaymentMode";
        //        ws.Cells["C" + rowStart].Style.Font.Bold = true;
        //        ws.Cells["C" + rowStart].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //        ws.Cells["C" + rowStart].Style.Fill.BackgroundColor.SetColor(drakGray);
        //        ws.Cells["C" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["C" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //        ws.Cells["C" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["C" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //        ws.Cells["C" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["C" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);

        //        ws.Cells["D" + rowStart].Value = "Consumer Number";
        //        ws.Cells["D" + rowStart].Style.Font.Bold = true;
        //        ws.Cells["D" + rowStart].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //        ws.Cells["D" + rowStart].Style.Fill.BackgroundColor.SetColor(drakGray);
        //        ws.Cells["D" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["D" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //        ws.Cells["D" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["D" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //        ws.Cells["D" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["D" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);

        //        ws.Cells["E" + rowStart].Value = "Receipt Reference";
        //        ws.Cells["E" + rowStart].Style.Font.Bold = true;
        //        ws.Cells["E" + rowStart].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //        ws.Cells["E" + rowStart].Style.Fill.BackgroundColor.SetColor(drakGray);
        //        ws.Cells["E" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["E" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //        ws.Cells["E" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["E" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //        ws.Cells["E" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["E" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);

        //        ws.Cells["F" + rowStart].Value = "Consumer Name";
        //        ws.Cells["F" + rowStart].Style.Font.Bold = true;
        //        ws.Cells["F" + rowStart].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //        ws.Cells["F" + rowStart].Style.Fill.BackgroundColor.SetColor(drakGray);
        //        ws.Cells["F" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["F" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //        ws.Cells["F" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["F" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //        ws.Cells["F" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["F" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);

        //        ws.Cells["G" + rowStart].Value = "Bill Amount";
        //        ws.Cells["G" + rowStart].Style.Font.Bold = true;
        //        ws.Cells["G" + rowStart].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //        ws.Cells["G" + rowStart].Style.Fill.BackgroundColor.SetColor(drakGray);
        //        ws.Cells["G" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["G" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //        ws.Cells["G" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["G" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //        ws.Cells["G" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["G" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);

        //        ws.Cells["H" + rowStart].Value = "Collection Amount";
        //        ws.Cells["H" + rowStart].Style.Font.Bold = true;
        //        ws.Cells["H" + rowStart].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //        ws.Cells["H" + rowStart].Style.Fill.BackgroundColor.SetColor(drakGray);
        //        ws.Cells["H" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["H" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //        ws.Cells["H" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["H" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //        ws.Cells["H" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["H" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);

        //        ws.Cells["I" + rowStart].Value = "Transaction Date-Time";
        //        ws.Cells["I" + rowStart].Style.Font.Bold = true;
        //        ws.Cells["I" + rowStart].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //        ws.Cells["I" + rowStart].Style.Fill.BackgroundColor.SetColor(drakGray);
        //        ws.Cells["I" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["I" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //        ws.Cells["I" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["I" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //        ws.Cells["I" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["I" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);

        //        ws.Cells["J" + rowStart].Value = "Status";
        //        ws.Cells["J" + rowStart].Style.Font.Bold = true;
        //        ws.Cells["J" + rowStart].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //        ws.Cells["J" + rowStart].Style.Fill.BackgroundColor.SetColor(drakGray);
        //        ws.Cells["J" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["J" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //        ws.Cells["J" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["J" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //        ws.Cells["J" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //        ws.Cells["J" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);

        //        rowStart++;
        //        int sNo = 1;

        //        foreach (var item in transData)
        //        {
        //            ws.Cells["A" + rowStart].Value = sNo;
        //            ws.Cells["A" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["A" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //            ws.Cells["A" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["A" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //            ws.Cells["A" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["A" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);
        //            ws.Cells["A" + rowStart].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["A" + rowStart].Style.Border.Bottom.Color.SetColor(Color.Black);

        //            ws.Cells["B" + rowStart].Value = item.TerminalId;
        //            ws.Cells["B" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["B" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //            ws.Cells["B" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["B" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //            ws.Cells["B" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["B" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);
        //            ws.Cells["B" + rowStart].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["B" + rowStart].Style.Border.Bottom.Color.SetColor(Color.Black);

        //            ws.Cells["C" + rowStart].Value = item.Paymentmode;
        //            ws.Cells["C" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["C" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //            ws.Cells["C" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["C" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //            ws.Cells["C" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["C" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);
        //            ws.Cells["C" + rowStart].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["C" + rowStart].Style.Border.Bottom.Color.SetColor(Color.Black);

        //            ws.Cells["D" + rowStart].Value = item.Reference1;
        //            ws.Cells["D" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["D" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //            ws.Cells["D" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["D" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //            ws.Cells["D" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["D" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);
        //            ws.Cells["D" + rowStart].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["D" + rowStart].Style.Border.Bottom.Color.SetColor(Color.Black);

        //            ws.Cells["E" + rowStart].Value = item.Rcptref;
        //            ws.Cells["E" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["E" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //            ws.Cells["E" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["E" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //            ws.Cells["E" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["E" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);
        //            ws.Cells["E" + rowStart].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["E" + rowStart].Style.Border.Bottom.Color.SetColor(Color.Black);

        //            ws.Cells["F" + rowStart].Value = item.Reference3;
        //            ws.Cells["F" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["F" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //            ws.Cells["F" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["F" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //            ws.Cells["F" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["F" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);
        //            ws.Cells["F" + rowStart].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["F" + rowStart].Style.Border.Bottom.Color.SetColor(Color.Black);

        //            ws.Cells["G" + rowStart].Value = item.Billamount;
        //            ws.Cells["G" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["G" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //            ws.Cells["G" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["G" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //            ws.Cells["G" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["G" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);
        //            ws.Cells["G" + rowStart].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["G" + rowStart].Style.Border.Bottom.Color.SetColor(Color.Black);

        //            ws.Cells["H" + rowStart].Value = item.Collectionamt;
        //            ws.Cells["H" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["H" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //            ws.Cells["H" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["H" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //            ws.Cells["H" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["H" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);
        //            ws.Cells["H" + rowStart].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["H" + rowStart].Style.Border.Bottom.Color.SetColor(Color.Black);

        //            ws.Cells["I" + rowStart].Value = item.Paymentdate;
        //            ws.Cells["I" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["I" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //            ws.Cells["I" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["I" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //            ws.Cells["I" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["I" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);
        //            ws.Cells["I" + rowStart].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["I" + rowStart].Style.Border.Bottom.Color.SetColor(Color.Black);

        //            ws.Cells["J" + rowStart].Value = item.Updationstatus;
        //            ws.Cells["J" + rowStart].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["J" + rowStart].Style.Border.Top.Color.SetColor(Color.Black);
        //            ws.Cells["J" + rowStart].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["J" + rowStart].Style.Border.Left.Color.SetColor(Color.Black);
        //            ws.Cells["J" + rowStart].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["J" + rowStart].Style.Border.Right.Color.SetColor(Color.Black);
        //            ws.Cells["J" + rowStart].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        //            ws.Cells["J" + rowStart].Style.Border.Bottom.Color.SetColor(Color.Black);

        //            rowStart++;
        //            sNo++;
        //        }

        //        ws.Cells["A:AZ"].AutoFitColumns();

        //        pck.Save();
        //        result = pck.GetAsByteArray();
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        Utility.WriteLog("Ledger Excel Report --> " + ex.Message);
        //        Utility.WriteLog("Ledger Excel Report --> " + ex.StackTrace);
        //        return result;
        //    }

        //}

        //public async Task<List<BatchCloseSummary>> BatchCloseData(ReportModel model)
        //{
        //    List<BatchCloseSummary> response = new List<BatchCloseSummary>();
        //    try
        //    {
        //        DateTime from = DateTime.ParseExact(model.FromDate.Replace('-', '/'), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        //        DateTime to = DateTime.ParseExact(model.ToDate.Replace('-', '/'), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        //        to = to.AddHours(23);
        //        to = to.AddMinutes(59);
        //        to = to.AddSeconds(59);

        //        response = await _PSPCL_LiveContext.BatchCloseSummary.Where(w => w.BatchCloseDateTime >= from && w.BatchCloseDateTime <= to).OrderByDescending(o => o.BatchCloseDateTime).ToListAsync();

        //        //return await _PSPCL_LiveContext.BatchCloseSummary.Where(w => w.BatchCloseDateTime >= from && w.BatchCloseDateTime <= to).OrderByDescending(o => o.BatchCloseDateTime).ToListAsync();
        //    }
        //    catch (Exception ex)
        //    {
        //        Utility.WriteLog("Batch Close Data --> " + ex.Message);
        //        Utility.WriteLog("Batch Close Data --> " + ex.StackTrace);
        //        //return new List<BatchCloseSummary>();
        //    }
        //    return response;
        //}

        public async Task<List<UnclearedSummary>> unclearedSummaryService()
        {
            List<UnclearedSummary> response = new List<UnclearedSummary>();
            try
            {
                response = await _PSPCL_LiveContext.Database.SqlQuery<UnclearedSummary>("sp_unclearedSummary").ToListAsync();
            }
            catch (Exception ex)
            {
                Utility.WriteLog("UnclearedSummaryService --> " + ex.Message);
                Utility.WriteLog("UnclearedSummaryService --> " + ex.StackTrace);
            }

            return response;
        }

        public async Task<byte[]> TransactionCSVReport(ReportModel model)
        {
            byte[] result = null;
            try
            {
                List<Transactions> transData = await Report(model);
                transData = transData.OrderBy(o => o.PAYMENTDATE).ToList();

                string reportFileName = $"{DateTime.Now.ToString("ddMMyyyyHHmmss")}TransactionReport.csv";

                var dirPath = AppDomain.CurrentDomain.BaseDirectory + "TransactionReport\\";
                var reportFilePath = AppDomain.CurrentDomain.BaseDirectory + "TransactionReport\\" + reportFileName;
                if (!Directory.Exists(dirPath)) Directory.CreateDirectory(dirPath).Create();

                FileInfo sInfo = new FileInfo(reportFilePath);

                FileStream fs = new FileStream(reportFilePath, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter strm = new StreamWriter(fs);

                strm.BaseStream.Seek(0, SeekOrigin.End);
                strm.WriteLine($"S.No. , Terminal Id , PaymentMode , Consumer Number , Receipt Reference , Consumer Name , Bill Amount , Collection Amount , Transaction Date-Time , Status ");
                strm.Flush();
                //-----------------------------------------------------------//
                int sNo = 1;
                foreach (var item in transData)
                {
                    strm.BaseStream.Seek(0, SeekOrigin.End);
                    strm.WriteLine($"{sNo},{item.TerminalID},{item.PAYMENTMODE},{item.REFERENCE1},{item.RCPTREF},{item.REFERENCE3},{item.BILLAMOUNT},{item.COLLECTIONAMT},{item.PAYMENTDATE},{item.UPDATIONSTATUS}");
                    strm.Flush();
                    sNo++;
                }
                strm.Close();

                //fs = new FileStream(reportFilePath, FileMode.Open, FileAccess.Read);
                //BinaryReader br = new BinaryReader(fs);
                //long numBytes = new FileInfo(reportFilePath).Length;
                //result = br.ReadBytes((int)numBytes);               

                result = File.ReadAllBytes(reportFilePath);
                return result;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Ledger Excel Report --> " + ex.Message);
                Utility.WriteLog("Ledger Excel Report --> " + ex.StackTrace);
                return result;
            }
        }

        //public async Task<List<kioskAttendanceDto>> LoginData(ReportModel model)
        //{
        //    //List<KioskAttendences> response = new List<KioskAttendences>();
        //    //List<KioskAttendences> response1 = new List<KioskAttendences>();
        //    //List<KioskAttendanceDto> kioskAttendanceDtos = new List<KioskAttendanceDto>();
        //    List<kioskAttendanceDto> response = new List<kioskAttendanceDto>();
        //    try
        //    {
        //        DateTime from = DateTime.ParseExact(model.FromDate.Replace('-', '/'), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        //        DateTime to = DateTime.ParseExact(model.ToDate.Replace('-', '/'), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        //        to = to.AddHours(23);
        //        to = to.AddMinutes(59);
        //        to = to.AddSeconds(59);

        //        response = await _PSPCL_LiveContext.Database.SqlQuery<kioskAttendanceDto>("sp_get_attendance @fromDate,@toDate", new SqlParameter("@fromDate", from), new SqlParameter("@toDate", to)).ToListAsync();

        //        response = response.OrderBy(o => o.KioskId).ThenBy(d => d.ReportDate).ToList();


        //        //response = await _PSPCL_LiveContext.KioskAttendences.Where(w => w.ActionDateTime >= from && w.ActionDateTime <= to).OrderByDescending(o => o.ActionDateTime).ToListAsync();

        //        ////var data = from resp in response
        //        ////           group resp by new
        //        ////           {
        //        ////               kioskId = resp.KioskId,
        //        ////               action = resp.Action
        //        ////           } into eGroup
        //        ////           orderby eGroup.Key
        //        ////           select eGroup;



        //        //response.ForEach(f =>
        //        //{
        //        //    response1.Add(new KioskAttendences
        //        //    {
        //        //        Action = f.Action,
        //        //        ActionDateTime = f.ActionDateTime,
        //        //        AtpmMachnIp = f.AtpmMachnIp,
        //        //        Id = f.Id,
        //        //        InsertedBy = f.InsertedBy,
        //        //        InsertedDate = f.InsertedDate,
        //        //        KioskId = f.KioskId,
        //        //        Status = f.Status,
        //        //        TokenId = f.TokenId,
        //        //        UpdatedOn = f.UpdatedOn,
        //        //    });
        //        //});

        //        //response.ForEach(f =>
        //        //{
        //        //    f.ActionDateTime = new DateTime(f.ActionDateTime.Value.Year, f.ActionDateTime.Value.Month, f.ActionDateTime.Value.Day, 0, 0, 0);
        //        //});

        //        //var data = response.GroupBy(resp => new
        //        //{
        //        //    KisoskId = resp.KioskId,
        //        //    DateTime = resp.ActionDateTime
        //        //}).Select(s => new
        //        //{
        //        //    KisoskId = s.Key.KisoskId,
        //        //    DateTime = s.Key.DateTime,
        //        //    DataList = response1.Where(w => w.KioskId == s.Key.KisoskId)
        //        //}).ToList();


        //        //data.ForEach(f =>
        //        //{
        //        //    KioskAttendanceDto kioskAttendanceDto = new KioskAttendanceDto
        //        //    {
        //        //        KioskId = f.KisoskId,
        //        //        ReportingDate = f.DateTime,
        //        //        LoginTime = f.DataList.Where(w => w.ActionDateTime.Value >= f.DateTime).OrderBy(o => o.ActionDateTime).FirstOrDefault(ff => ff.Action.ToUpper() == "LOGIN") != null ? f.DataList.Where(w => w.ActionDateTime.Value >= f.DateTime).OrderBy(o => o.ActionDateTime).FirstOrDefault(ff => ff.Action.ToUpper() == "LOGIN").ActionDateTime : null,
        //        //        LogoutTime = f.DataList.Where(w => w.ActionDateTime.Value <= f.DateTime.Value.AddHours(23).AddMinutes(59).AddSeconds(59)).FirstOrDefault(ff => ff.Action.ToUpper() == "LOGOUT") != null ? f.DataList.Where(w => w.ActionDateTime.Value <= f.DateTime.Value.AddHours(23).AddMinutes(59).AddSeconds(59)).FirstOrDefault(ff => ff.Action.ToUpper() == "LOGOUT").ActionDateTime : null
        //        //    };

        //        //    kioskAttendanceDtos.Add(kioskAttendanceDto);
        //        //});

        //        //kioskAttendanceDtos = kioskAttendanceDtos.OrderBy(o => o.KioskId).ThenBy(d => d.ReportingDate).ToList();

        //        //return await _PSPCL_LiveContext.BatchCloseSummary.Where(w => w.BatchCloseDateTime >= from && w.BatchCloseDateTime <= to).OrderByDescending(o => o.BatchCloseDateTime).ToListAsync();
        //    }
        //    catch (Exception ex)
        //    {
        //        Utility.WriteLog("loginData --> " + ex.Message);
        //        Utility.WriteLog("loginData --> " + ex.StackTrace);
        //        //return new List<BatchCloseSummary>();
        //    }
        //    return response;
        //}

        public async Task<List<SpUserDetail>> UserList()
        {
            var result = await _PSPCL_LiveContext.Database.SqlQuery<SpUserDetail>("SpUserView").ToListAsync();
            return result.Where(w => w.Role.ToUpper() != RoleType.SuperAdmin).ToList();
        }

        public async Task<ApplicationUser> GetUserDetail(string uniqueId)
        {
            return await _PSPCL_LiveContext.Users.FirstOrDefaultAsync(f => f.Id == uniqueId);
        }

        public async Task<List<SpUserDetail>> SubAdminList()
        {
            var result = await _sqlService.GetUsers();
            return result.Where(w => w.Role.ToUpper() == RoleType.SubAdmin).ToList();
        }

        public async Task<CommonDto> UpdateUser(AddUserModel model)
        {
            try
            {
                var updateData = await _PSPCL_LiveContext.Users.FirstOrDefaultAsync(f => f.Id == model.Id);

                updateData.Location = model.Location;
                updateData.ModifiedBy = model.LoginUsername;
                updateData.ModifiedOn = DateTime.Now;
                updateData.Name = model.Name;
                updateData.Email = model.Email;
                updateData.PhoneNumber = model.Phone;

                await _PSPCL_LiveContext.SaveChangesAsync();

                return new CommonDto
                {
                    IsSuccess = true,
                    Message = "User Updated Successfully"
                };
            }
            catch (Exception ex)
            {
                return new CommonDto
                {
                    IsSuccess = false,
                    Message = $"Error :- {ex.Message}"
                };
            }
        }

        public async Task<CommonDto> ActiveUser(string uniqueId, string loginUser)
        {
            try
            {
                var updateData = await _PSPCL_LiveContext.Users.FirstOrDefaultAsync(f => f.Id == uniqueId);

                updateData.Status = (int)UserStatus.Active;
                updateData.ModifiedBy = loginUser;
                updateData.ModifiedOn = DateTime.Now;

                await _PSPCL_LiveContext.SaveChangesAsync();

                // Code for send Email
                EmailModel emailRequest = new EmailModel
                {
                    EmailSubject = "Account Activation",
                    MessageBody = $"Hello {updateData.Name}, Your account is successfully activated. Kindly login with following credentials :- Username :- {updateData.UserName} and Password :- {SecurityHelper.Decrypt(updateData.Password)}",
                    ToAddress = updateData.Email
                };

                await _emailSmsService.SendEmail(JsonConvert.SerializeObject(emailRequest));

                // Code for send Sms
                SmsModel smsRequest = new SmsModel
                {
                    MessageBody = $"Hello {updateData.Name}, Your account is successfully activated. Kindly login with following credentials :- Username :- {updateData.UserName} and Password :- {SecurityHelper.Decrypt(updateData.Password)}",
                    MobileNumber = new List<string> { updateData.PhoneNumber }
                };

                await _emailSmsService.SendSms(JsonConvert.SerializeObject(smsRequest));

                return new CommonDto
                {
                    IsSuccess = true,
                    Message = "User is Activated Successfully"
                };
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Active User --> " + ex.Message);
                Utility.WriteLog("Active User --> " + ex.StackTrace);

                return new CommonDto
                {
                    IsSuccess = false,
                    Message = $"Error :- {ex.Message}"
                };
            }

        }

        public async Task<CommonDto> InActiveUser(string uniqueId, string loginUser)
        {
            try
            {
                var updateData = await _PSPCL_LiveContext.Users.FirstOrDefaultAsync(f => f.Id == uniqueId);

                updateData.Status = (int)UserStatus.Inactive;
                updateData.ModifiedBy = loginUser;
                updateData.ModifiedOn = DateTime.Now;

                await _PSPCL_LiveContext.SaveChangesAsync();

                return new CommonDto
                {
                    IsSuccess = true,
                    Message = "User DeActivated Successfully"
                };
            }
            catch (Exception ex)
            {
                Utility.WriteLog("InActive User --> " + ex.Message);
                Utility.WriteLog("InActive User --> " + ex.StackTrace);

                return new CommonDto
                {
                    IsSuccess = false,
                    Message = $"Error : {ex.Message}"
                };
            }
        }

        public async Task<List<DataPoints>> TransactionSummary(ReportModel model)
        {
            List<DataPoints> dataPoints = new List<DataPoints>();
            try
            {
                DateTime from = DateTime.ParseExact(model.FromDate.Replace('-', '/'), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime to = DateTime.ParseExact(model.ToDate.Replace('-', '/'), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                to = to.AddHours(23);
                to = to.AddMinutes(59);
                to = to.AddSeconds(59);

                try
                {
                    List<ReportSummaryDto> summary = new List<ReportSummaryDto>();
                    summary = await _PSPCL_LiveContext.Database.SqlQuery<ReportSummaryDto>($"EXEC sp_transactions_summary @fromDate,@toDate",
                            new Object[]{ new SqlParameter("@fromDate",from),
                                      new SqlParameter("@toDate",to)}).ToListAsync();

                    foreach (var item in summary)
                    {
                        dataPoints.Add(new DataPoints(item.TerminalId, item.Count, ""));
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog("AdminService", $"TransactionCount -> {ex.Message}\n{ex.StackTrace}");
                    //throw;
                }
                //return summary;
                //return await _adaniContext.Transactions.Where(w => w.Mode == "L" && w.Paymentdate >= from && w.Paymentdate <= to).GroupBy(g => g.TerminalId).Select(s => new ReportSummaryModel { Count = s.Count(), TerminalId = s.Key }).ToListAsync();
            }
            catch (Exception ex)
            {
                Utility.WriteLog("TransactionCount --> " + ex.Message);
                Utility.WriteLog("TransactionCount --> " + ex.StackTrace);
                //return new List<ReportSummaryModel>();
            }
            return dataPoints;
        }

        public async Task<List<PSPCL_KSK_BATCH_DTLS>> BatchCloseData(ReportModel model)
        {
            try
            {
                DateTime from = DateTime.ParseExact(model.FromDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
                DateTime to = DateTime.ParseExact(model.ToDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
                to = to.AddHours(23);
                to = to.AddMinutes(59);
                to = to.AddSeconds(59);

                return await _PSPCL_LiveContext.PSPCL_KSK_BATCH_DTLS.Where(w => w.Batch_Close_Date >= from && w.Batch_Close_Date <= to).OrderByDescending(o => o.Batch_Close_Date).ToListAsync();
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Batch Close Data --> " + ex.Message);
                Utility.WriteLog("Batch Close Data --> " + ex.StackTrace);

                return new List<PSPCL_KSK_BATCH_DTLS>();
            }
        }

        public async Task<CommonDto> TransactionCsvReport(ReportModel model, CancellationToken cancellationToken)
        {
            try
            {
                List<SpTransactionDto> transData = await ReportData(model);
                transData = transData.OrderBy(o => o.TransactionDate).ToList();

                StringBuilder stringBuilder = new StringBuilder();


                string reportFileName = $"{DateTime.Now.ToString("ddMMyyyyHHmmss")}TransactionReport.csv";
                string dirPath = AppDomain.CurrentDomain.BaseDirectory + "TransactionReport\\";
                string reportFolderName = "TransactionReport\\" + reportFileName;
                string reportFilePath = AppDomain.CurrentDomain.BaseDirectory + reportFolderName;

                if (!Directory.Exists(dirPath)) Directory.CreateDirectory(dirPath).Create();

                string header = string.Join(",",
                                       "Terminal Id",
                                       "Usp",
                                       "Type",
                                       "Consumer Number",
                                       "Receipt Reference",
                                       "DocumentId",
                                       "Consumer Name",
                                       "Bill Amount",
                                       "Collection Amount",
                                       "Transaction Date-Time",
                                       "Status",
                                       "Receipt_Number",
                                       "BatchClose_DateTime"
                                       );

                stringBuilder.AppendLine(header);

                transData.ForEach(f =>
                {
                    string content = string.Join(",", f.TerminalId,
                                                      f.Usp,
                                                      f.Type,
                                                      f.ConsumerNumber,
                                                      f.ReceiptRef,
                                                      f.Document_Id,
                                                      f.ConsumerName,
                                                      f.BillAmount,
                                                      f.CollectionAmt,
                                                      f.TransactionDate,
                                                      f.Updationstatus,
                                                      f.ReceiptNumber,
                                                      f.ReportBatchNo
                                                      );
                    stringBuilder.AppendLine(content);
                });


                File.WriteAllText(reportFilePath, stringBuilder.ToString());

                return new CommonDto
                {
                    IsSuccess = true,
                    Response = reportFolderName,
                    Message = reportFolderName.Replace(".csv", "")
                };
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Ledger Excel Report --> " + ex.Message);
                Utility.WriteLog("Ledger Excel Report --> " + ex.StackTrace);
                return new CommonDto
                {

                };
            }

        }

        public async Task<List<SpTransactionDto>> ReportData(ReportModel model)
        {
            try
            {
                var spResult = await _sqlService.TransactionReport(model);

                //if (model.LoginUserRole.ToUpper() != RoleType.Admin)
                //    spResult = spResult.Where(w => w.Usp.ToUpper() != "AEPS").ToList();

                List<SpTransactionDto> transactionsData = new List<SpTransactionDto>();

                switch (model.Type)
                {
                    case "ALL":
                        if (model.TerminalId != null)
                            transactionsData = spResult.Where(w => w.TerminalId == model.TerminalId).OrderByDescending(o => o.TransactionDate).ToList();
                        else
                            transactionsData = spResult;
                        break;

                    default:
                        if (model.TerminalId != null)
                            transactionsData = spResult.Where(w => w.Type == model.Type && w.TerminalId == model.TerminalId).OrderByDescending(o => o.TransactionDate).ToList();
                        else
                            transactionsData = spResult.Where(w => w.Type == model.Type).OrderByDescending(o => o.TransactionDate).ToList();
                        break;
                }
                transactionsData = transactionsData.OrderBy(t => t.TerminalId).ThenByDescending(d => d.TransactionDate).ToList();
                return transactionsData;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Report --> " + ex.Message);
                Utility.WriteLog("Report --> " + ex.StackTrace);
                return new List<SpTransactionDto>();
            }
        }

        public async Task<List<BatchCloseReview>> BatchCloseReview()
        {
            List<BatchCloseReview> response = new List<BatchCloseReview>();
            try
            {
                response = await _PSPCL_LiveContext.Database.SqlQuery<BatchCloseReview>("Get_PSPCL_Last_Batch_Report").ToListAsync();
            }
            catch (Exception ex)
            {
                Utility.WriteLog("BatchCloseReview --> " + ex.Message);
                Utility.WriteLog("BatchCloseReview --> " + ex.StackTrace);
            }

            return response;
        }

        public async Task<List<LineChartDataPoints>> monthlyTranTrend()
        {
            List<monthlyTranTrend> response = new List<monthlyTranTrend>();
            List<LineChartDataPoints> dataPoints = new List<LineChartDataPoints>();
            try
            {
                response = await _PSPCL_LiveContext.Database.SqlQuery<monthlyTranTrend>("sp_get_transactions_summary_monthly_trend").ToListAsync();
                foreach (var item in response)
                {
                    LineChartDataPoints lineChartDataPoints = new LineChartDataPoints
                    {
                        label = item.TraxMonth,
                        y = item.Tranx,
                        PayMode = item.Mop
                    };
                    dataPoints.Add(lineChartDataPoints);
                }

                //response = response.GroupBy(x=>x.TraxMonth).Select()
            }
            catch (Exception ex)
            {
                Utility.WriteLog("monthlyTranTrend --> " + ex.Message);
                Utility.WriteLog("monthlyTranTrend --> " + ex.StackTrace);
            }
            return dataPoints;
        }

        public async Task<List<GetTrxnHistoryDto>> ReportDataPagination(ReportPaginationModel model)
        {
            try
            {
                var spResult = await _sqlService.TransactionReportPagination(model);

                //if (model.LoginUserRole.ToUpper() != RoleType.Admin)
                //    spResult = spResult.Where(w => w.Usp.ToUpper() != "AEPS").ToList();

                List<GetTrxnHistoryDto> transactionsData = new List<GetTrxnHistoryDto>();

                switch (model.PaymentMode)
                {
                    case "ALL":
                        if (model.TerminalId != null)
                            transactionsData = spResult.Where(w => w.TerminalID == model.TerminalId).OrderByDescending(o => o.TransactionDate).ToList();
                        else
                            transactionsData = spResult;
                        break;

                    default:
                        if (model.TerminalId != null)
                            transactionsData = spResult.Where(w => w.Type == model.PaymentMode && w.TerminalID == model.TerminalId).OrderByDescending(o => o.TransactionDate).ToList();
                        else
                            transactionsData = spResult.Where(w => w.Type == model.PaymentMode).OrderByDescending(o => o.TransactionDate).ToList();
                        break;
                }
                transactionsData = transactionsData.OrderBy(t => t.TerminalID).ThenByDescending(d => d.TransactionDate).ToList();
                return transactionsData;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Report --> " + ex.Message);
                Utility.WriteLog("Report --> " + ex.StackTrace);
                return new List<GetTrxnHistoryDto>();
            }
        }

        public async Task<List<PayFileDaywiseSummary>> GenerateNonSapPayFileSummary(ReportModel model)
        {
            DateTime from = DateTime.ParseExact(model.FromDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
            DateTime to = DateTime.ParseExact(model.ToDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
            to = to.AddHours(23);
            to = to.AddMinutes(59);
            to = to.AddSeconds(59);
            string currentDate = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            //List<NonSapPayFileSummaryDto> dto = new List<NonSapPayFileSummaryDto>();
            //List<NonSapDayWiseSummary> dto = new List<NonSapDayWiseSummary>();
            List<PayFileDaywiseSummary> dto = new List<PayFileDaywiseSummary>();
            try
            {
                dto = await _sqlService.GEtPayFileSummary(model);
                //dto.ForEach(x =>
                //{
                //    x.DS = x.DS ?? 0;
                //    x.SP = x.SP ?? 0;
                //    x.MS = x.MS ?? 0;
                //    x.LS = x.LS ?? 0;
                //    x.GC = x.GC ?? 0;
                //    x.Solor = x.Solor ?? 0;
                //    x.Tem = x.Tem ?? 0;                    
                //});

                //dto.ForEach(x => x.Total = x.MS + x.DS + x.LS + x.GC + x.Solor + x.Tem + x.SP);
                var data = await _sqlService.GetPaymentFileData(model);

                if (data.Count > 0)
                {
                    //data.ForEach(x =>
                    //{
                    //    if (x.Category == null) x.Category = x.CustomerId.Substring(3, 2);
                    //    if (x.ReceiptNumber > 100000) x.ReceiptNumber = x.ReceiptNumber % 100000;
                    //});

                    data.ForEach(x =>
                    {
                        x.Category = x.Category ?? x.CustomerId.Substring(3, 2);
                        x.ReceiptNumber = x.ReceiptNumber > 100000 ? x.ReceiptNumber % 100000 : x.ReceiptNumber;
                    });

                    /*
                    var summary = data.GroupBy(g => g.Category)
                        .Select(lg => new
                        {
                            Category = lg.Key,
                            Bills = lg.Count(),
                            CollectionAmt = lg.Sum(s => s.PaidAmount)
                        });
                    */
                    //var summary = data.GroupBy(g => new { g.ReportDate, g.Category })
                    //    .Select(lg => new
                    //    {
                    //        ReportDate = lg.Key.ReportDate,
                    //        Category = lg.Key.Category,
                    //        Bills = lg.Count(),
                    //        CollectionAmt = lg.Sum(s => s.PaidAmount)
                    //    }).ToList();

                    string filename = $"{model.TerminalId}\\{from.ToString("yyyyMMdd")}-{to.ToString("yyyyMMdd")}";

                    var categoryList = data.Select(x => x.Category).Distinct();
                    //string subDivision = data.FirstOrDefault(s => s.Subdivision);
                    foreach (var item in categoryList)
                    {
                        string result = Utility.WritePaymentFile(filename, data.Where(x => x.Category == item).ToList());
                    }



                    string DownloadFilePath = $"{AppDomain.CurrentDomain.BaseDirectory}OutputFiles\\{filename}";

                    string zipFilePath = $"{AppDomain.CurrentDomain.BaseDirectory}OutputFiles\\";
                    string DescFilePath = $"{AppDomain.CurrentDomain.BaseDirectory}SentFiles\\";
                    DirectoryInfo directory = new DirectoryInfo($"{zipFilePath}");
                    string[] sourceFiles = Directory.GetFiles(zipFilePath);

                    foreach (string sourceFile in sourceFiles)
                    {
                        string fileName = Path.GetFileName(sourceFile);
                        string destFile = Path.Combine(DescFilePath, fileName);
                        if (System.IO.File.Exists(destFile))
                            System.IO.File.Delete(destFile);
                        System.IO.File.Move(sourceFile, destFile);
                    }

                    using (ZipArchive newFile = ZipFile.Open($"{zipFilePath}\\{model.TerminalId}-{from.ToString("yyyyMMdd")}-{to.ToString("yyyyMMdd")}.zip", ZipArchiveMode.Create))
                    {
                        foreach (string file in Directory.GetFiles(DownloadFilePath))
                        {
                            newFile.CreateEntryFromFile(file, System.IO.Path.GetFileName(file));
                            File.Delete(file);
                        }
                    }

                    /*
                    foreach (var item in summary)
                    {
                        NonSapDayWiseSummary record = new NonSapDayWiseSummary
                        {
                            TerminalId = model.TerminalId,
                            ReportDate = item.ReportDate,
                            Category = item.Category,
                            Bills = item.Bills,
                            CollectionAmt = item.CollectionAmt
                        };
                        dto.Add(record);
                    }
                    */
                    //foreach (var item in summary)
                    //{
                    //    NonSapPayFileSummaryDto records = new NonSapPayFileSummaryDto
                    //    {
                    //        TerminalId = model.TerminalId,
                    //        FromDate = from.ToString("yyyy-MM-dd HH:mm:ss"),
                    //        ToDate = to.ToString("yyyy-MM-dd HH:mm:ss"),
                    //        Category = item.Category,
                    //        Bills = item.Bills,
                    //        CollectionAmt = item.CollectionAmt
                    //    };
                    //    dto.Add(records);
                    //}
                }
                return dto;
            }
            catch (Exception ex)
            {
                NonSapPayFileSummaryDto records = new NonSapPayFileSummaryDto
                {
                    TerminalId = model.TerminalId,
                    FromDate = from.ToString("yyyy-MM-dd HH:mm:ss"),
                    ToDate = to.ToString("yyyy-MM-dd HH:mm:ss"),
                    Category = "Not Found",
                    Bills = 0,
                    CollectionAmt = 0
                };
                return dto;
            }
        }

        public async Task<string> GenerateNonSapPaymentFile()
        {
            //DateTime from = DateTime.ParseExact(model.FromDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
            //DateTime to = DateTime.ParseExact(model.ToDate.Replace('-', '/'), "yyyy/MM/dd", CultureInfo.InvariantCulture);
            //to = to.AddHours(23);
            //to = to.AddMinutes(59);
            //to = to.AddSeconds(59);
            string currentDate = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            CommonDto dto = new CommonDto();
            //List<byte[]> bytesList = new List<byte[]>();
            //byte[] bytes = null;
            //List<string> f_name = new List<string>();
            string zipFilePath = $"{AppDomain.CurrentDomain.BaseDirectory}OutputFiles\\";
            string DownloadFilePath = string.Empty;
            try
            {
                //var data = await _sqlService.GetPaymentFileData(model);
                //string filename = $"{model.TerminalId}\\{from.ToString("yyyyMMdd")}-{to.ToString("yyyyMMdd")}";

                //var categoryList = data.Select(x => x.Category).Distinct();
                //string subDivision = data.FirstOrDefault(s => s.Subdivision);
                //foreach (var item in categoryList)
                //{
                //    string result = Utility.WritePaymentFile(filename, data.Where(x => x.Category == item).ToList());
                //}

                //DownloadFilePath = $"{AppDomain.CurrentDomain.BaseDirectory}OutputFiles\\{filename}";

                //string zipFilePath = $"{AppDomain.CurrentDomain.BaseDirectory}OutputFiles\\";
                //using (ZipArchive newFile = ZipFile.Open($"{zipFilePath}\\{currentDate}_dump.zip", ZipArchiveMode.Create))
                //{
                //    foreach (string file in Directory.GetFiles(DownloadFilePath))
                //    {
                //        newFile.CreateEntryFromFile(file, System.IO.Path.GetFileName(file));
                //        File.Delete(file);
                //    }
                //}

                //string zipFilePath = $"{AppDomain.CurrentDomain.BaseDirectory}OutputFiles\\";
                DirectoryInfo directory = new DirectoryInfo($"{zipFilePath}");
                FileInfo[] fileInfo = directory.GetFiles("*.zip");

                DownloadFilePath = $"{AppDomain.CurrentDomain.BaseDirectory}OutputFiles\\{fileInfo[0]}";


                //foreach (var file in fileInfo)
                //{
                //    using (FileStream fileStream = new FileStream($"{zipFilePath}\\{file.Name}", FileMode.Open, FileAccess.Read))
                //    {
                //        bytes = File.ReadAllBytes($"{zipFilePath}\\{file.Name}");
                //        fileStream.Read(bytes, 0, Convert.ToInt32(fileStream.Length));
                //        fileStream.Close();
                //    }
                //    f_name.Add(file.Name);
                //    bytesList.Add(bytes);
                //    //bytes = null;
                //}

                //dto = new CommonDto
                //{
                //    IsSuccess = true,
                //    Response = zipFilePath,
                //    Message = zipFilePath.Replace(".zip", "")
                //};            
            }
            catch (Exception ex)
            {
                dto = new CommonDto
                {
                    Response = ex,
                    Message = ex.Message
                };
            }
            return DownloadFilePath;
        }

        public async Task<List<string>> GetNonSapSiteList()
        {
            List<string> SiteList = new List<string>();
            try
            {
                //var SiteList = await _PSPCL_LiveContext.Transactions.Where(x => x.REFERENCEINT1 == 1).Select(s => s.TerminalID).Distinct().ToListAsync();

                //var nonSapCntrs = await _PSPCL_LiveContext.Transactions.Join(_PSPCL_LiveContext.SiteList, T1 => T1.TerminalID, T2 => T2.MachineID, (T1, T2) => new { T1, T2 }).Where(w => w.T1.REFERENCEINT1 == 1).Select(s => new                 {
                //    kioskID = s.T1.TerminalID,
                //    Location = s.T2.City
                //}).Distinct().OrderBy(o => o.kioskID).ToListAsync();
                var nonSapCntrs = _PSPCL_LiveContext.Database.SqlQuery<nonSapkiosks>("Sp_Get_NonSapSites").OrderBy(o => o.KioskID).ToList();

                foreach (var site in nonSapCntrs)
                {
                    SiteList.Add(site.KioskID + " - " + site.Location);
                }

                return SiteList;
            }
            catch (Exception ex)
            {
                //List<string> SiteList = new List<string>();
                SiteList.Add($"{ex.Message}");
                return SiteList;
            }

        }
    }
}
