﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Service.Area.ApiManager.Interface
{
    public interface IApiManagerService
    {
        Task<HttpResponseMessage> GetAsync(string baseUrl, string url);
        Task<HttpResponseMessage> PostAsync(string baseUrl, string url, StringContent stringContent);
    }
}
