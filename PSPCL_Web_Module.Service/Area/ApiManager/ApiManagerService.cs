﻿using PSPCL_Web_Module.Service.Area.ApiManager.Interface;
using PSPCL_Web_Module.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Service.Area.ApiManager
{
    public class ApiManagerService : IApiManagerService
    {
        public async Task<HttpResponseMessage> GetAsync(string baseUrl, string url)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Clear();

                EnableHost.EnableTrustedHosts();
                ////make sure to use TLS 1.2 first before trying other version
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                //set Accept headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml,application/json");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{Utility.ApiUsername}:{Utility.ApiPassword}")));

                //set User agent
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; EN; rv:11.0) like Gecko");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

                var response = await client.GetAsync(url);
                return response;
            }
        }

        public async Task<HttpResponseMessage> PostAsync(string baseUrl, string url, StringContent stringContent)
        {
            using (var client = new HttpClient())
            {
                EnableHost.EnableTrustedHosts();
                ////make sure to use TLS 1.2 first before trying other version
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                client.BaseAddress = new Uri(baseUrl);

                ////set Accept headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml,application/json");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{Utility.ApiUsername}:{Utility.ApiPassword}")));
                //set User agent
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; EN; rv:11.0) like Gecko");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

                var response = await client.PostAsync(url, stringContent);
                return response;
            }
        }
    }
}
