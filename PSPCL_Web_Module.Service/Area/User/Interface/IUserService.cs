﻿using PSPCL_Web_Module.Dto.Areas.Common;
using PSPCL_Web_Module.InfraStructure.EF.Entity;
using PSPCL_Web_Module.Models.Area.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Service.Area.User.Interface
{
    public interface IUserService
    {
        Task<List<Transactions>> Report(string terminalId);
        Task<List<Transactions>> Report(ReportModel model);
        Task<CommonDto> BatchClose(string terminalId);
        Task<string> BatchCloseStatus(string terminalId);
    }
}
