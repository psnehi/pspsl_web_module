﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PSPCL_Web_Module.Dto.Areas.Common;
using PSPCL_Web_Module.InfraStructure.EF.Entity;
using PSPCL_Web_Module.Models.Area.Admin;
using PSPCL_Web_Module.Service.Area.User;
using PSPCL_Web_Module.Service.Area.User.Interface;

namespace PSPCL_Web_Module.Service.Area.User
{
    public class UserService : IUserService
    {
        public Task<CommonDto> BatchClose(string terminalId)
        {
            throw new NotImplementedException();
        }

        public Task<string> BatchCloseStatus(string terminalId)
        {
            throw new NotImplementedException();
        }

        public Task<List<Transactions>> Report(string terminalId)
        {
            throw new NotImplementedException();
        }

        public Task<List<Transactions>> Report(ReportModel model)
        {
            throw new NotImplementedException();
        }
    }
}
