﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Service.Area.EmailSms.Interface
{
    public interface IEmailSmsService
    {
        Task SendEmail(string json);
        Task SendSms(string json);
    }
}
