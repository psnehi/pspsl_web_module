﻿using PSPCL_Web_Module.Service.Area.ApiManager.Interface;
using PSPCL_Web_Module.Service.Area.EmailSms.Interface;
using PSPCL_Web_Module.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Service.Area.EmailSms
{
    public class EmailSmsService : IEmailSmsService
    {
        private readonly IApiManagerService _apiService;
        private readonly string _baseUrl = Urls.EmailSmsBaseUrl;
        public EmailSmsService(IApiManagerService apiService)
        {
            _apiService = apiService;
        }

        public async Task SendEmail(string json)
        {
            try
            {
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await _apiService.PostAsync(_baseUrl, Urls.Email, content);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                //Reading response as string
                string responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                Utility.WriteLog("SendEmail --> " + ex.Message);
                Utility.WriteLog("SendEmail --> " + ex.StackTrace);
            }
        }

        public async Task SendSms(string json)
        {
            try
            {
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await _apiService.PostAsync(_baseUrl, Urls.Sms, content);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                //Reading response as string
                string responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                Utility.WriteLog("SendSms--> " + ex.Message);
                Utility.WriteLog("SendSms--> " + ex.StackTrace);
            }
        }
    }
}
