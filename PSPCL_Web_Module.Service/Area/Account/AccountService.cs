﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PSPCL_Web_Module.Dto.Areas.Common;
using PSPCL_Web_Module.Models.Area.Account;
using PSPCL_Web_Module.Service.Area.Account.Interface;
using PSPCL_Web_Module.InfraStructure.EF;
using PSPCL_Web_Module.InfraStructure.EF.Entity;
using PSPCL_Web_Module.Utilities;
using System.Data.Entity;


namespace PSPCL_Web_Module.Service.Area.Account
{
    public class AccountService : IAccountService
    {
        private readonly PSPCL_LiveContext _dataContext;
        public AccountService(PSPCL_LiveContext dataContext)
        {
            _dataContext = dataContext;
        }

        //public async Task<CommonDto> Account(LoginModel model)
        //{
        //    CommonDto dto = new CommonDto();
        //    try
        //    {
        //        var account = await _dataContext.UserMaster.FirstOrDefaultAsync(f => f.UserName.ToUpper() == model.Username.ToUpper() && f.Password == model.Password);
        //        if (account != null)
        //        {
        //            dto = new CommonDto
        //            {
        //                IsSuccess = true,
        //                Message = "Login Successfully",
        //                Response = account
        //            };
        //        }
        //        else
        //        {
        //            dto = new CommonDto
        //            {
        //                IsSuccess=false,
        //                Message="Invalid Username or Password"
        //            };
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        Utility.WriteLog("AccountErrorLog", $"ExceptionMessage-> {ex.Message}\nStackTrace-> {ex.StackTrace}");
        //        dto = new CommonDto
        //        {
        //            IsSuccess = false,
        //            Message = "Server Error, Please try after sometimes "
        //        };
        //        //throw new NotImplementedException();
        //    }
        //    return dto;
        //}

        public Task<CommonDto> ChangePassword(ChangePasswordModel model)
        {
            throw new NotImplementedException();
        }
    }
}
