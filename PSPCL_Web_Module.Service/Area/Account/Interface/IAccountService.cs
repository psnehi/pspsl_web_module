﻿using PSPCL_Web_Module.Dto.Areas.Common;
using PSPCL_Web_Module.Models.Area.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Service.Area.Account.Interface
{
    public interface IAccountService
    {
        //Task<CommonDto> Account(LoginModel model);
        Task<CommonDto> ChangePassword(ChangePasswordModel model);
    }
}
