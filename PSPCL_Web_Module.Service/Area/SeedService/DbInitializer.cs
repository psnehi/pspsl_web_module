﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PSPCL_Web_Module.InfraStructure.EF;
using PSPCL_Web_Module.InfraStructure.EF.Entity;
using PSPCL_Web_Module.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Claims;

namespace PSPCL_Web_Module.Service.Area.SeedService
{
    public class DbInitializer
    {
        public static void Initialize(PSPCL_LiveContext context)
        {
            try
            {
                if (!context.Roles.Any())
                {
                    var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

                    List<IdentityRole> roles = new List<IdentityRole>
                    {
                        new IdentityRole{ Name=RoleType.SuperAdmin},
                        new IdentityRole{ Name = RoleType.Admin},
                        new IdentityRole{ Name=RoleType.SubAdmin},
                        new IdentityRole{ Name = RoleType.User}
                    };
                    foreach (var role in roles)
                    {
                        roleManager.Create(role);
                    }
                }

                if (!context.Users.Any())
                {
                    var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                    // string password = "password";

                    //var user = new ApplicationUser
                    //{
                    //    Email = "prashant@tsiplc.com",
                    //    EmailConfirmed = true,
                    //    PhoneNumber = "9069056902",
                    //    PhoneNumberConfirmed = true,
                    //    UserName = "prashant@tsiplc.com",
                    //    Name = "Prashant Kumar Snehi",
                    //    Password = SecurityHelper.Encrypt(password),
                    //    Status = (int)UserStatus.Active,
                    //    CreatedBy = "System",
                    //    CreatedOn = DateTime.Now,
                    //    Location = "New Delhi"
                    //};

                    List<ApplicationUser> listData = new List<ApplicationUser>
                    {
                        new ApplicationUser
                        {
                            Email = "prashant@tsiplc.com",
                            EmailConfirmed = true,
                            PhoneNumber = "9810013306",
                            PhoneNumberConfirmed = true,
                            UserName = "prashant",
                            Name = "Prashant Kumar Snehi",
                            Password = SecurityHelper.Encrypt("password"),
                            Status = (int)UserStatus.Active,
                            CreatedBy = "System",
                            CreatedOn = DateTime.Now,
                            Location = "New Delhi"
                        },
                        new ApplicationUser
                        {
                            Email = "psnehi@yahoo.com",
                            EmailConfirmed = true,
                            PhoneNumber = "9810013306",
                            PhoneNumberConfirmed = true,
                            UserName = "admin",
                            Name = "Prashant Kumar Snehi",
                            Password = SecurityHelper.Encrypt("password"),
                            Status = (int)UserStatus.Active,
                            CreatedBy = "System",
                            CreatedOn = DateTime.Now,
                            Location = "New Delhi"
                        }
                    };

                    List<string> roles = new List<string>
                    {                        
                        "SUPER ADMIN",                     
                        "ADMIN"
                    };



                    for (int i = 0; i < listData.Count; i++)
                    {
                        var result = userManager.Create(listData[i], "password");

                        if (result.Succeeded)
                        {
                            switch (roles[i].ToUpper())
                            {
                                case RoleType.SuperAdmin:
                                    userManager.AddToRole(listData[i].Id, RoleType.SuperAdmin);
                                    userManager.AddClaim(listData[i].Id, new Claim(ClaimTypes.Role, RoleType.SuperAdmin));
                                    break;

                                case RoleType.Admin:
                                    userManager.AddToRole(listData[i].Id, RoleType.Admin);
                                    userManager.AddClaim(listData[i].Id, new Claim(ClaimTypes.Role, RoleType.Admin));
                                    break;

                                case RoleType.SubAdmin:
                                    userManager.AddToRole(listData[i].Id, RoleType.SubAdmin);
                                    userManager.AddClaim(listData[i].Id, new Claim(ClaimTypes.Role, RoleType.SubAdmin));
                                    break;

                                case RoleType.User:
                                    userManager.AddToRole(listData[i].Id, RoleType.User);
                                    userManager.AddClaim(listData[i].Id, new Claim(ClaimTypes.Role, RoleType.User));
                                    break;
                            }
                        }
                    }

                    //  var result = userManager.Create(user, password);


                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("ApplicationStartup", $"Exception Message: {ex.Message}\nInnerException: {ex.InnerException}\nStack Trace: {ex.StackTrace}");
                throw;
            }
        }
    }
}
