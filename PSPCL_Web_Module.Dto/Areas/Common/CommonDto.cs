﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Dto.Areas.Common
{
    public class CommonDto
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public object Response { get; set; }
    }
}
