﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Dto.Areas.Admin
{
    public class NonSapDayWiseSummary
    {
        public string TerminalId { get; set; }
        public string ReportDate { get; set; }
        public string Category { get; set; }
        public int Bills { get; set; }
        public decimal CollectionAmt { get; set; }
    }
}
