﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Dto.Areas.Admin
{
    public class GetTrxnHistoryDto
    {
        public int Id { get; set; }
        public string TerminalID { get; set; }
        public string KioskAddress { get; set; }
        public string Type { get; set; }
        public string PayMode { get; set; }
        public string UpdationMessage { get; set; }
        public string ConsumerName { get; set; }
        public string ChequeNo { get; set; }
        public string MICR { get; set; }
        public string BillAmount { get; set; }
        public string CollectionAmount { get; set; }
        public DateTime TransactionDate { get; set; }
        public string UpdationStatus { get; set; }
        public int ReceiptNumber { get; set; }
        public string ReportBatchNo { get; set; }
    }
}
