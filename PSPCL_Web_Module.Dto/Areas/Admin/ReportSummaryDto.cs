﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Dto.Areas.Admin
{
    public class ReportSummaryDto
    {
        public string TerminalId { get; set; }
        public int Count { get; set; }
    }
}
