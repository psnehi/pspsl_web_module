﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Dto.Areas.Admin
{
    public class PayFileDaywiseSummary
    {
        public string ReportDate { get; set; }
        public decimal Ds_Bills     { get; set; }
        public decimal DS_Amount    { get; set; }
        public decimal SP_Bills     { get; set; }
        public decimal SP_Amount    { get; set; }
        public decimal MS_Bills     { get; set; }
        public decimal MS_Amount    { get; set; }
        public decimal LS_Bills     { get; set; }
        public decimal LS_Amount    { get; set; }
        public decimal GC_Bills     { get; set; }
        public decimal GC_Amount    { get; set; }
        public decimal Solor_Bills  { get; set; }
        public decimal Solor_Amount { get; set; }
        public decimal Tem_Bills    { get; set; }
        public decimal Tem_Amount   { get; set; }
        public decimal Total_Bills  { get; set; }
        public decimal Total_Amount { get; set; }


    }
}
