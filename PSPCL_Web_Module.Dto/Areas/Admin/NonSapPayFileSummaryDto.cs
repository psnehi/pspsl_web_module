﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Dto.Areas.Admin
{
    public class NonSapPayFileSummaryDto
    {
        public string TerminalId { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string Category { get; set; }
        public int Bills { get; set; }
        public decimal CollectionAmt { get; set; }
    }
}
