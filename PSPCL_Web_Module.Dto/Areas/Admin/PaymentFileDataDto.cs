﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Dto.Areas.Admin
{
    public class PaymentFileDataDto
    {
        public string TerminalId { get; set; }
        public string CustomerId { get; set; }
        public string Subdivision { get; set; }
        public string Category { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal PaidAmount { get; set; }
        public int ReceiptNumber { get; set; }
        public string ReportDate { get; set; }
    }
}
