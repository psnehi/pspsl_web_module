﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Dto.Areas.Admin
{
    public class TransactionDetailCountModel
    {
        public int TodayCount { get; set; }
        public int MonthCount { get; set; }
        public int YearCount { get; set; }
    }
}
