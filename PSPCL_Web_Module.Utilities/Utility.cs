﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PSPCL_Web_Module.Dto.Areas.Admin;

namespace PSPCL_Web_Module.Utilities
{
    public class Utility
    {
        public static string ApiUsername { get; } = ConfigurationManager.AppSettings["ApiUsername"];
        public static string ApiPassword { get; } = ConfigurationManager.AppSettings["ApiPassword"];

        public static void WriteLog(string message)
        {
            var dirPath = AppDomain.CurrentDomain.BaseDirectory + "Logs\\";

            if (!Directory.Exists(dirPath)) Directory.CreateDirectory(dirPath).Create();

            var filePath = $"{dirPath}log_{DateTime.Now:MMddyyyy}.txt".Replace("/", "");

            if (!File.Exists(filePath))
            {
                File.Create(filePath).Dispose();
            }

            using (var w = File.AppendText(filePath))
            {
                w.WriteLine("---------------------------");
                w.WriteLine("[{0:MM/dd/yyyy hh:mm:ss tt}]: {1}", DateTime.Now, message);
                w.WriteLine("---------------------------");
                w.Close();
            }
        }


        public static void WriteLog(string fileName, string message)
        {
            var dirPath = AppDomain.CurrentDomain.BaseDirectory + "Logs\\";

            if (!Directory.Exists(dirPath)) Directory.CreateDirectory(dirPath).Create();

            var filePath = $"{dirPath}{fileName}_Log_{DateTime.Now:MMddyyyy}.txt".Replace("/", "");

            if (!File.Exists(filePath))
            {
                File.Create(filePath).Dispose();
            }

            using (var w = File.AppendText(filePath))
            {
                w.WriteLine("---------------------------");
                w.WriteLine("[{0:MM/dd/yyyy hh:mm:ss tt}]: {1}", DateTime.Now, message);
                w.WriteLine("---------------------------");
                w.Close();
            }
        }

        public static string WritePaymentFile(string fileName, List<PaymentFileDataDto> data)
        {
            var fileCredentials = data.Select(x => new
            {
                x.Subdivision,
                x.Category,
                x.TerminalId
            }).FirstOrDefault();

            var fromDate = data.Min(x => x.PaymentDate);
            var toDate = data.Max(x => x.PaymentDate);
            var dirPath = $"{AppDomain.CurrentDomain.BaseDirectory}OutputFiles\\{fileName}";

            if (!Directory.Exists(dirPath)) Directory.CreateDirectory(dirPath).Create();
            var filePath = $"{dirPath}\\{fileCredentials.Subdivision}-{fileCredentials.Category}-{fileCredentials.TerminalId}-{fromDate.ToString("yyyyMMdd")}-{toDate.ToString("yyyyMMdd")}.txt";
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                File.Create(filePath).Dispose();
            }
            foreach (var item in data)
            {
                if (item.Category != "LS")
                {
                    using (var w = File.AppendText(filePath))
                    {
                        w.WriteLine($"{item.Subdivision}EB51{item.CustomerId.Trim().PadLeft(12, '0')}{((long)item.PaidAmount).ToString().PadLeft(7, '0')}1{item.PaymentDate.ToString("ddMMyyyy")}{item.TerminalId.Substring(2, 4)}{item.ReceiptNumber.ToString().PadLeft(5, '0')} S000");
                        w.Close();
                    }
                }
                else
                {
                    string filler = string.Empty;
                    using (var w = File.AppendText(filePath))
                    {
                        w.WriteLine($"{filler.PadLeft(6, ' ')}{item.PaymentDate.ToString("HH:mm:ss")}{item.CustomerId.PadLeft(12, ' ')}{filler.PadLeft(1, ' ')}{((long)item.PaidAmount).ToString().PadLeft(9, ' ')}{item.PaymentDate.ToString("yyyyMMdd")}0{item.TerminalId.Substring(2, 4)}{item.ReceiptNumber.ToString().PadLeft(5, '0')}");
                        w.Close();
                    }
                }

            }

            if (!File.Exists(filePath))
            {
                File.Create(filePath).Dispose();
            }

            return "Success";
        }
    }

   

    public static class RequestStatus  // Top-Up Request Status
    {
        public const int Pending = 1;
        public const int Approved = 2;
        public const int Rejected = 3;

    }

    public static class RoleType
    {
        public const string SuperAdmin = "SUPER ADMIN";
        public const string Admin = "ADMIN";
        public const string SubAdmin = "SUB ADMIN";
        public const string User = "USER";
    }

    public enum UserStatus
    {
        Legacy = 1,
        Pending = 2,
        Incomplete = 3,
        Active = 4,
        Inactive = 5,
        Excluded = 6,
        Deactivated = 7
    }

    public class SqlConstant
    {
        public const string SpUserView = "SpUserView";
        public const string SpTransactionReport = "sp_get_transactionsReport_new @fromDate,@toDate";
        public const string SpGetTrxnHistory = "getTrxnHistory @terminalId,@fromDate,@toDate,@consumerNumber,@paymentMode,@page,@size,@sortColumn,@sortOrder,@totalRows";
        public const string SpGetNonSapTrxnForPaymentFile = "sp_nonsap_payment_files @terminalId,@fromDate,@todate";
        public const string SpGetNonSapPayFilesSummary = "sp_nonsap_payment_files_summary @terminalId,@fromDate,@todate";
    }
}
