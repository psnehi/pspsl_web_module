﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace PSPCL_Web_Module.Utilities
{
    public static class EmailSmsConfig
    {
        public static string CcAddresses => ConfigurationManager.AppSettings["CcAddresses"];
        public static string BccAddresses => ConfigurationManager.AppSettings["BccAddresses"];
        public static string ToAddress => ConfigurationManager.AppSettings["ToAddress"];
        public static string MobileNumbers => ConfigurationManager.AppSettings["MobileNumbers"];
    }
}
