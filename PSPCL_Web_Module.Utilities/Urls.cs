﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Utilities
{
    public static class Urls
    {
        public static string BaseUrl => ConfigurationManager.AppSettings["BaseUrl"];
        public static string AgencyId => ConfigurationManager.AppSettings["AgencyId"];
        public static string EmailSmsBaseUrl => ConfigurationManager.AppSettings["EmailSmsBaseUrl"];

        public const string Email = "api/Communication/SendWebPortalEmail";
        public const string Sms = "api/Communication/SendSMS";
        public static string MongoDbConnectionString => ConfigurationManager.AppSettings["MongoDbConnectionString"];
    }
}
