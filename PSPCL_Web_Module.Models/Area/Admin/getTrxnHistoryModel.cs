﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Models.Area.Admin
{
    public class GetTrxnHistoryModel
    {
        public string TerminalId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string consumerNumber { get; set; }
        public string paymentMode { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
        public string SortColumn { get; set; }
        public string SortOrder { get; set; }
        public int TotalRecords { get; set; }

    }
}
