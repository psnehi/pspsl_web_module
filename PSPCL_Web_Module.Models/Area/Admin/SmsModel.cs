﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Models.Area.Admin
{
    public class SmsModel
    {
        public List<string> MobileNumber { get; set; }
        public string MessageBody { get; set; }
    }
}
