﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Models.Area.Admin
{
    public class BatchCloseReview
    {
        public string Terminal_Id { get; set; }
        public string Location { get; set; }
        public string Division { get; set; }
        public string Address { get; set; }
        public int Last_batch { get; set; }
        public DateTime STARTDATE { get; set; }
        public DateTime ENDDATE { get; set; }
        public int Total_Receipts { get; set; }
        public string Total_Amount { get; set; }
        public int Pending_Days { get; set; }

    }
}
