﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PSPCL_Web_Module.Models.Area.Admin
{
    public class DataPoints
    {
        public DataPoints()
        {

        }

        public DataPoints(string label, double y, string paymode)
        {
            this.label = label;
            this.y = y;
            this.PayMode = paymode;
        }

        [JsonProperty(PropertyName = "label")]
        public string label = "";
        //public int Tranx { get; set; }
        [JsonProperty(PropertyName = "y")]
        public Nullable<double> y = null;

        [JsonProperty(PropertyName = "y")]
        public string PayMode = "";
    }
}
