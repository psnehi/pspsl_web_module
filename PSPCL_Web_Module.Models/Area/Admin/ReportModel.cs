﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Models.Area.Admin
{
    public class ReportModel : BaseModel
    {
        public ReportModel()
        {
            //SColumnsNames = new List<string>();
            //BSortable = new List<bool>();
            //BSearchable = new List<bool>();
            //SSearchValues = new List<string>();
            //ISortCol = new List<int>();
            //SSortDir = new List<string>();
            //BEscapeRegexColumns = new List<bool>();
        }

        public string Type { get; set; }
        public string TerminalId { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        //public int Start { get; set; }
        //public int Length { get; set; }
        //public string SearchValue { get; set; }
        //public string SortColumnName { get; set; }
        //public string SortDirection { get; set; }

        //[JsonProperty("iDisplayStart")]
        //public int IDisplayStart { get; set; }
        //[JsonProperty("iDisplayLength")]
        //public int IDisplayLength { get; set; }
        //[JsonProperty("iColumns")]
        //public int IColumns { get; set; }
        //[JsonProperty("sSearch")]
        //public string SSearch { get; set; }
        //[JsonProperty("bEscapeRegex")]
        //public bool BEscapeRegex { get; set; }
        //[JsonProperty("iSortingCols")]
        //public int ISortingCols { get; set; }
        //[JsonProperty("sEcho")]
        //public int SEcho { get; set; }
        //[JsonProperty("sColumnsNames")]
        //public List<string> SColumnsNames { get; set; }
        //[JsonProperty("bSortable")]
        //public List<bool> BSortable { get; set; }
        //[JsonProperty("bSearchable")]
        //public List<bool> BSearchable { get; set; }
        //[JsonProperty("sSearchValues")]
        //public List<string> SSearchValues { get; set; }
        //[JsonProperty("iSortCol")]
        //public List<int> ISortCol { get; set; }
        //[JsonProperty("sSortDir")]
        //public List<string> SSortDir { get; set; }
        //[JsonProperty("bEscapeRegexColumns")]
        //public List<bool> BEscapeRegexColumns { get; set; }
        //[JsonProperty("iSortCol_0")]
        //public string ISortCol_0 { get; set; }
        //[JsonProperty("sSortDir_0")]
        //public string SSortDir_0 { get; set; }
    }
}
