﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Models.Area.Admin
{
    public class SpTransactionDto
    {
        public int SNo { get; set; }
        public string TerminalId { get; set; }
        public string Usp { get; set; }
        public string Type { get; set; }
        public string ConsumerNumber { get; set; }
        public string ReceiptRef { get; set; }
        public string Document_Id { get; set; }
        public string ConsumerName { get; set; }
        public decimal BillAmount { get; set; }
        public decimal CollectionAmt { get; set; }
        public string TransactionDate { get; set; }                
        public string Updationstatus { get; set; }
        public decimal ReceiptNumber { get; set; }
        public long ReportBatchNo { get; set; }
        public string UpdationMessage { get; set; }
        //public Nullable<DateTime> BatchCloseDate { get; set; }
    }
}
