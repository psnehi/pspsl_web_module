﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Models.Area.Admin
{
    public class UnclearedSummary
    {
        public string KioskId { get; set; }
        public string KioskLocation { get; set; }
        //public DateTime Since { get; set; }        
        public Nullable<decimal> Cash { get; set; }
        public Nullable<decimal> Cheque { get; set; }
        public Nullable<decimal> Card { get; set; }

        //public int Tranx { get; set; }  
        //public decimal CollectionAmt { get; set; }
        //public string KioskId { get; set; }
        //public string KioskLocation { get; set; }
        //public DateTime Since { get; set; }        
        //public string Paymentmode { get; set; }
        //public int Tranx { get; set; }  
        //public decimal CollectionAmt { get; set; }
    }
}
