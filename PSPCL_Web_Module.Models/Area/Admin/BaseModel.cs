﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Models.Area.Admin
{
    public class BaseModel
    {
        public string LoginUsername { get; set; }
        public string LoginName { get; set; }
        public string LoginUserRole { get; set; }
    }
}
