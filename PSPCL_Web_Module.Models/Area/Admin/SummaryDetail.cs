﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSPCL_Web_Module.Models.Area.Admin
{
    public class SummaryDetail
    {
        public string TerminalId { get; set; }
        public string Paymentmode { get; set; }
        public int Tranx { get; set; }
        public decimal CollectionAmt { get; set; }
    }
}
