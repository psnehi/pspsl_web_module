﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PSPCL_Web_Module.Startup))]
namespace PSPCL_Web_Module
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
