﻿using PSPCL_Web_Module.Dto.Areas.Common;
using PSPCL_Web_Module.InfraStructure.EF;
using PSPCL_Web_Module.InfraStructure.EF.Entity;
using PSPCL_Web_Module.Models.Area.Account;
using PSPCL_Web_Module.Service.Area.Account;
using PSPCL_Web_Module.Service.Area.Account.Interface;
using PSPCL_Web_Module.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PSPCL_Web_Module.Areas.Account.Controllers
{
    public class AccountController : Controller
    {

        private readonly IAccountService _accountService;
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private readonly PSPCL_LiveContext _dataContext;



        public AccountController(IAccountService accountService,
                                 ApplicationUserManager userManager,
                                 ApplicationSignInManager signInManager,
                                 PSPCL_LiveContext dataContext)
        {
            _accountService = accountService;
            _userManager = userManager;
            _signInManager = signInManager;
            _dataContext = dataContext;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        // GET: Account/Account
        [HttpGet]
        public ActionResult Account(string returnUrl = null)

        {
            ViewBag.ReturnUrl = returnUrl;
            return View("/Areas/Account/Views/Account/Account.cshtml");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Account(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await SignInManager.PasswordSignInAsync(model.Username, model.Password, false, shouldLockout: false);

                if (result == SignInStatus.Success)
                {
                    var user = await _dataContext.Users.FirstOrDefaultAsync(x => x.UserName == model.Username);
                    var claims = await UserManager.GetClaimsAsync(user.Id);

                    if (claims.Any(c => c.Type == ClaimTypes.Role && c.Value == RoleType.Admin) || claims.Any(c => c.Type == ClaimTypes.Role && c.Value == RoleType.SuperAdmin) || claims.Any(c => c.Type == ClaimTypes.Role && c.Value == RoleType.SubAdmin))
                    {
                        return RedirectToAction("Index", "Admin", new { Area = "Admin" });
                    }
                    else
                    {
                        return RedirectToAction("Index", "User", new { Area = "User" });
                    }
                }
                else if (result == SignInStatus.Failure)
                {
                    ViewData["LoginErrors"] = "Invalid Login Credentials";
                    return View("/Areas/Account/Views/Account/Account.cshtml", model);
                }
                else
                {
                    ViewData["LoginErrors"] = "Invalid Login Credentials";
                    return View("/Areas/Account/Views/Account/Account.cshtml", model);
                }
            }
            else
            {
                ViewData["LoginErrors"] = "Login Credentials is required";
                return View("/Areas/Account/Views/Account/Account.cshtml", model);
            }
        }

        [HttpGet]
        [Authorize]
        public ActionResult ChangePasswordView() => View();

        [Authorize]
        [HttpPost]
        public async Task<JsonResult> ChangePassword(ChangePasswordModel model)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = await _dataContext.Users.FirstOrDefaultAsync(f => f.Id == userId);
                if (user == null || !await UserManager.CheckPasswordAsync(user, model.OldPassword))
                    return Json(new CommonDto { IsSuccess = false, Message = "Old password does not match." });

                var result = await UserManager.ChangePasswordAsync(user.Id, model.OldPassword, model.NewPassword);
                if (result.Succeeded)
                {
                    user.Password = SecurityHelper.Encrypt(model.NewPassword);
                    await _dataContext.SaveChangesAsync();
                    return Json(new CommonDto { IsSuccess = true, Message = "Password changed successfully." });
                }
                else
                {
                    return Json(new CommonDto { IsSuccess = false, Message = result.Errors.ElementAtOrDefault(0) });
                }
            }
            catch (Exception ex)
            {
                return Json(new CommonDto { IsSuccess = false, Message = $"Error : {ex.Message}" });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Account", "Account", new { Area = "Account" });
        }
    }
}