﻿using Microsoft.AspNet.Identity;
using PSPCL_Web_Module.InfraStructure.EF.Entity;
using PSPCL_Web_Module.Models.Area.Admin;
using PSPCL_Web_Module.Service.Area.Admin.Interface;
using PSPCL_Web_Module.Service.Area.SqlService.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PSPCL_Web_Module.Areas.Admin.Controllers
{
    [Authorize(Roles = "SUPER ADMIN, ADMIN, SUB ADMIN")]
    public class AdminController : Controller
    {
        private readonly IAdminService _adminService;
        private readonly ISqlService _sqlService;

        public AdminController(IAdminService adminService,
                               ISqlService sqlService)
        {
            _adminService = adminService;
            _sqlService = sqlService;
        }

        // GET: Admin/Admin
        public ActionResult Index() => View("/Areas/Admin/Views/Admin/Index.cshtml");

        [HttpPost]
        public async Task<JsonResult> TransactionCount(ReportModel model) => Json(await _adminService.TransactionCount(model), JsonRequestBehavior.AllowGet);

        [HttpGet]
        public async Task<JsonResult> getTransactionsCount(ReportModel model) => Json(await _adminService.TransactionCount(model), JsonRequestBehavior.AllowGet);

        [HttpPost]
        public async Task<JsonResult> TransactionSummary(ReportModel model) => Json(await _adminService.TransactionSummary(model), JsonRequestBehavior.AllowGet);

        [HttpGet]
        public async Task<JsonResult> TransactionDetail() => Json(await _adminService.TransactionDetail(), JsonRequestBehavior.AllowGet);

        [HttpGet]
        public async Task<JsonResult> GetTotalTransaction() => Json(await _adminService.TotalTransactionDetail(), JsonRequestBehavior.AllowGet);

        [HttpGet]
        public ActionResult AddUserView() => View();

        [HttpPost]
        public async Task<JsonResult> AddUser(AddUserModel model)
        {
            model.LoginUsername = ((ClaimsIdentity)User.Identity).FindFirstValue(ClaimTypes.Sid);
            model.LoginName = ((ClaimsIdentity)User.Identity).FindFirstValue(ClaimTypes.Name);
            return Json(await _adminService.AddUser(model));
        }

        [HttpGet]
        public async Task<ActionResult> GetAllUser() => View(await _adminService.UserList());

        [HttpGet]
        public async Task<ActionResult> GetAllSubAdmin() => View(await _adminService.SubAdminList());

        [HttpGet]
        public async Task<JsonResult> ActiveUser(string uniqueId) => Json(await _adminService.ActiveUser(uniqueId, ((ClaimsIdentity)User.Identity).FindFirstValue(ClaimTypes.Sid)), JsonRequestBehavior.AllowGet);
        [HttpGet]
        public async Task<JsonResult> InActiveUser(string uniqueId) => Json(await _adminService.InActiveUser(uniqueId, ((ClaimsIdentity)User.Identity).FindFirstValue(ClaimTypes.Sid)), JsonRequestBehavior.AllowGet);

        //[HttpGet]
        //public async Task<JsonResult> UpdateUserView(string uniqueId) => Json(await _adminService.GetUserDetail(uniqueId));
        public async Task<PartialViewResult> UpdateUserView(string uniqueId) => PartialView("_UpdateUserView", await _adminService.GetUserDetail(uniqueId));

        [HttpPost]
        public async Task<JsonResult> UpdateUserDetail(AddUserModel model)
        {
            model.LoginUsername = ((ClaimsIdentity)User.Identity).FindFirstValue(ClaimTypes.Sid);
            return Json(await _adminService.UpdateUser(model));
        }

        [HttpGet]
        public ActionResult AddSiteView() => View();

        [HttpPost]
        public async Task<JsonResult> AddSite(SiteList model) => Json(await _adminService.AddSite(model));

        [HttpGet]
        public async Task<ActionResult> SiteList() => View(await _adminService.SiteList());

        [HttpGet]
        public async Task<JsonResult> LiveSite(string SiteID, string MachineID) => Json(await _adminService.LiveSite(SiteID, MachineID), JsonRequestBehavior.AllowGet);

        [HttpGet]
        public async Task<JsonResult> StopSite(string SiteID, string MachineID) => Json(await _adminService.StopSite(SiteID, MachineID), JsonRequestBehavior.AllowGet);

        [HttpGet]
        public async Task<PartialViewResult> GetSiteDetail(string SiteID, string MachineID) => PartialView("_SiteDetail", await _adminService.SiteDetail(SiteID, MachineID));

        [HttpPost]
        public async Task<JsonResult> EditSiteDetail(SiteList model) => Json(await _adminService.UpdateSite(model), JsonRequestBehavior.AllowGet);

        [HttpGet]
        public ActionResult BatchCloseView() => View();

        [HttpPost]
        public async Task<PartialViewResult> BatchCloseData(ReportModel model)
        {
            Session.Remove("BatchCloseReportModel");
            Session["BatchCloseReportModel"] = model;
            return PartialView("_BatchCloseData", await _adminService.BatchCloseData(model));
        }

        [HttpPost]
        public async Task<JsonResult> TransactionSummaryDetail(ReportModel model) => Json(await _adminService.TransactionSummaryDetail(model), JsonRequestBehavior.AllowGet);

        /// <summary>
        /// Report Models
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> ReportView() => View(await _adminService.SiteId());

        [HttpGet]
        public async Task<ActionResult> GetHistoricalDataForm() => PartialView("_HistoricalDataForm", await _adminService.SiteId());

        //[HttpPost]
        //public async Task<ActionResult> Report(ReportModel model)
        //{
        //    model.LoginUserRole = ((ClaimsIdentity)User.Identity).FindFirstValue(ClaimTypes.Role);
        //    Session.Remove("TransactionReportModel");
        //    Session["TransactionReportModel"] = model;

        //    //var jsonResult = Json(await _adminService.Report(model), JsonRequestBehavior.AllowGet);
        //    //jsonResult.MaxJsonLength = int.MaxValue;
        //    //return jsonResult;            
        //    return PartialView("_Report", await _adminService.ReportData(model));
        //}        

        //[HttpPost]
        //public async Task<JsonResult> Report(ReportModel model) => Json(await _adminService.ReportData(model), JsonRequestBehavior.AllowGet);

        [HttpPost]
        public async Task<JsonResult> Report(ReportModel model)
        {
            try
            {
                return new JsonResult()
                {
                    Data = await _adminService.ReportData(model),
                    MaxJsonLength = int.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [AsyncTimeout(600000)]
        public async Task<JsonResult> TransactionCsvReport(ReportModel model, CancellationToken cancellationToken)
        {
            var linkedToken = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken, Response.ClientDisconnectedToken).Token;

            model.LoginUsername = ((ClaimsIdentity)User.Identity).FindFirstValue(ClaimTypes.Sid);
            model.LoginUserRole = ((ClaimsIdentity)User.Identity).FindFirstValue(ClaimTypes.Role);
            return Json(await _adminService.TransactionCsvReport(model, linkedToken), JsonRequestBehavior.AllowGet);
        }

        //[HttpGet]
        //public async Task<ActionResult> BatchCloseReview() => View(await _adminService.BatchCloseReview());

        [HttpGet]
        public async Task<ActionResult> BatchCloseReview()
        {
            try
            {
                return View(await _adminService.BatchCloseReview());
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //return View(await _adminService.BatchCloseReview());
        }

        [HttpGet]
        public async Task<JsonResult> monthlyTranTrend()
        {
            try
            {
                return new JsonResult()
                {
                    Data = await _adminService.monthlyTranTrend(),
                    MaxJsonLength = int.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public async Task<JsonResult> ReportPagination(ReportPaginationModel model)
        {
            try
            {
                return new JsonResult()
                {
                    Data = await _adminService.ReportDataPagination(model),
                    MaxJsonLength = int.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public async Task<ActionResult> NonSapPaymentFileView() => View(await _adminService.GetNonSapSiteList());

        [HttpPost]
        public async Task<JsonResult> GetNonSapPayFileSummary(ReportModel model)
        {
            try
            {
                if (model.TerminalId != null)
                {
                    string[] site = model.TerminalId.Split('-');
                    model.TerminalId = site[0].Trim();
                }

                return new JsonResult()
                {
                    Data = await _adminService.GenerateNonSapPayFileSummary(model),
                    MaxJsonLength = int.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public async Task<ActionResult> GenerateNonSapPaymentFile()
        {
            try
            {
                var file = await _adminService.GenerateNonSapPaymentFile();

                if (file != "")
                {
                    //string filename = $"{DateTime.Now.ToString("yyyyMMdd")}_dump.zip";
                    string filename = Path.GetFileName(file);
                    byte[] fileBytes = System.IO.File.ReadAllBytes(file);
                    //string fileName = $"_dump.zip";
                    string SrcFilePath = $"{AppDomain.CurrentDomain.BaseDirectory}OutputFiles\\";
                    string DescFilePath = $"{AppDomain.CurrentDomain.BaseDirectory}SentFiles\\";

                    if (!Directory.Exists(DescFilePath)) Directory.CreateDirectory(DescFilePath);
                    DirectoryInfo directory = new DirectoryInfo($"{DescFilePath}");
                    string[] sourceFiles = Directory.GetFiles(SrcFilePath);

                    foreach (string sourceFile in sourceFiles)
                    {
                        string fileName = Path.GetFileName(sourceFile);
                        string destFile = Path.Combine(DescFilePath, fileName);
                        if (System.IO.File.Exists(destFile))
                            System.IO.File.Delete(destFile);
                        System.IO.File.Move(sourceFile, destFile);
                    }
                    //return new FileContentResult(fileBytes, "application/zip");
                    return File(fileBytes, "application/zip", filename);
                }
                else
                {
                    return View("~/Areas/Admin/Views/Admin/NonSapPaymentFileView.cshtml", await _adminService.GetNonSapSiteList());
                }

            }
            catch (Exception ex)
            {
                return View("~/Areas/Admin/Views/Admin/NonSapPaymentFileView.cshtml", await _adminService.GetNonSapSiteList());
            }

            //try
            //{
            //    return new JsonResult()
            //    {
            //        Data = await _adminService.GenerateNonSapPaymentFile(model),
            //        MaxJsonLength = int.MaxValue,
            //        JsonRequestBehavior = JsonRequestBehavior.AllowGet
            //    };
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
        }
    }
}