using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using PSPCL_Web_Module.InfraStructure.EF;
using PSPCL_Web_Module.InfraStructure.EF.Entity;
using PSPCL_Web_Module.Service.Area.Account;
using PSPCL_Web_Module.Service.Area.Account.Interface;
using PSPCL_Web_Module.Service.Area.Admin;
using PSPCL_Web_Module.Service.Area.Admin.Interface;
using PSPCL_Web_Module.Service.Area.ApiManager;
using PSPCL_Web_Module.Service.Area.ApiManager.Interface;
using PSPCL_Web_Module.Service.Area.EmailSms;
using PSPCL_Web_Module.Service.Area.EmailSms.Interface;
using PSPCL_Web_Module.Service.Area.SqlService;
using PSPCL_Web_Module.Service.Area.SqlService.Interface;
using PSPCL_Web_Module.Service.Area.User;
using PSPCL_Web_Module.Service.Area.User.Interface;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using Unity;
using Unity.Injection;
using Unity.Mvc5;

namespace PSPCL_Web_Module
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<DbContext, PSPCL_LiveContext>();
            container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>();
            container.RegisterType<IAuthenticationManager>(new InjectionFactory(o => HttpContext.Current.GetOwinContext().Authentication));

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IApiManagerService, ApiManagerService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IAdminService, AdminService>();
            container.RegisterType<IAccountService, AccountService>();
            container.RegisterType<ISqlService, SqlService>();
            container.RegisterType<IEmailSmsService, EmailSmsService>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}