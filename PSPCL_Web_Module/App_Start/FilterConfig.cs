﻿using System.Web;
using System.Web.Mvc;

namespace PSPCL_Web_Module
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
